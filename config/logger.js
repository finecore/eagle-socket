const winston = require("winston");

require("winston-daily-rotate-file");
require("date-utils");

const logger = winston.createLogger({
  level: "debug", // 최소 레벨
  // 파일저장
  transports: [
    new winston.transports.DailyRotateFile({
      filename: `log/%DATE%-app.log`,
      datePattern: "YYYY-MM-DD", // 시간별 분리
      zippedArchive: false,
      maxSize: "10m",
      maxFiles: "30d",
      format: winston.format.printf((info) => `${new Date().toFormat("YYYY-MM-DD HH24:MI:SS")} [${info.level.toUpperCase()}] - ${info.message}`),
    }),
    // 콘솔 출력
    // new winston.transports.Console({
    //   format: winston.format.printf((info) => `${new Date().toFormat("YYYY-MM-DD HH24:MI:SS")} [${info.level.toUpperCase()}] - ${info.message}`),
    // }),
  ],
});

logger.br = (line) => {
  logger.debug("\r\n");
};

logger.substr = (data, len = 200) => {
  if (!data) return "";

  let logdate = typeof data === "object" ? JSON.stringify(data) : data;

  return logdate.length > len
    ? logdate.substring(0, len / 2) + "\n    .............................. skip ...............................  \n" + logdate.substring(logdate.length - len / 2, logdate.length - 1)
    : logdate;
};

module.exports = logger;
