const path = require("path");

module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: ["./src/index.js"],
  output: {
    filename: "./build/build.js",
    path: path.resolve(__dirname)
  },
  serve: {}
};
