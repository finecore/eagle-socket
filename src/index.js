const express = require("express");

const chalk = require("chalk");
const moment = require("moment");
const _ = require("lodash");
const ip = require("ip");

const { spawn } = require("child_process");

const stackTrace = require("stack-trace");

// winston log
const logger = require("../config/logger");

const SocketHelper = require("./helper/socket-helper");
const { send } = require("./utils/mail-util");
const { verify, sign } = require("./utils/jwt-util");

require("dotenv").config();

const config = {};

_.map(process.env, (v, k) => {
  // console.log(v, k);
  let chr = k.charAt(0);
  if (chr === chr.toUpperCase()) config[k] = v;
});

// console.log("=== CONFIG === ", config);

const app = express();

const PORT = 3000;
const IP = process.env.REACT_APP_MODE === "LOCAL" ? ip.address() : process.env.REACT_APP_EXT_IP; // 서버 외부 IP.

let isWriteLog = false;
let lastMailContent = "";
let lastLogData = [];
let mailSendDelay = true;
let logTimer = null;
let lastContent = "";

let helper = new SocketHelper();

app.listen(PORT, () => {
  console.info("======== socket alive check server started =========");
  console.info("Listening on " + process.env.REACT_APP_MODE, IP, PORT);
  console.info("====================================================\n");

  setTimeout(() => {
    mailSendDelay = false;

    let content =
      moment().format("YYYY-MM-DD HH:mm:ss") + "<br/><br/><strong>Socket Server Started!</strong><br/><br/>SVR: " + process.env.REACT_APP_SVR_NAME + "<br/>Ip: " + IP + "<br/>Port: " + PORT;

    sendErrMail("info@icrew.kr", "Server Started!", content);
  }, 5 * 1000);

  makeToken((token) => {
    console.log("- token ", token);

    const data = {
      headers: {
        method: "get",
        url: "server/list/" + `name='${process.env.REACT_APP_SVR_NAME}'`,
        channel: "socket",
        token,
      },
      body: {
        device: {
          connect: 1, // 0:정상, 1:비정상
        },
      },
    };

    setInterval(() => {
      getServerInfo(data);
    }, 60 * 1000);

    getServerInfo(data);
  });

  // 시간 타이머. (서버 시작시 바로 시작 금지!)
  this.timer = setInterval(() => {
    let HHmm = moment().format("HHmmss");

    // console.log("- Server Restart Timer ", HHmm);

    // 매일 새벽 5 시에 재기동 한다.
    if (HHmm === "050000") {
      // console.log("- Server Restart Time!");
      // spawn(process.argv[1], process.argv.slice(2), {
      //   detached: true,
      //   stdio: ["ignore"],
      // }).unref();
      // process.exit();
    }
  }, 1000);
});

const getServerInfo = (data) => {
  console.log("- getServerInfo ", data);

  try {
    // API 서버 조회 한다.
    helper.route(data, (err, res) => {
      console.log("- api call res server ", { err, res });
      if (!err && res.body) {
        let { name, status, write_log } = res.body.servers[0];

        if (write_log && !isWriteLog) {
          console.info("- isWriteLog start", { name, status });
          isWriteLog = true;
        } else if (!write_log && isWriteLog) {
          console.info("- isWriteLog stop", { name, status });
          isWriteLog = false;
        }
      } else {
        console.error("- getServerInfo error", err);
      }
    });
  } catch (e) {
    console.error("- getServerInfo error catch", err);
  }
};

const makeToken = (callback) => {
  // JWT(Json Web Token) 생성.
  const payload = {
    channel: "socket",
    place_id: 1,
    mode: process.env.REACT_APP_MODE,
  };

  sign(payload, (err, token) => {
    if (err) console.error("-- makeToken error", err);
    callback(token);
  });
};

const addLastLog = (log) => {
  if (log) {
    lastLogData.push(log);
    if (lastLogData.length > 50) lastLogData.shift();
  }
};

const sendErrMail = (from, subject, content) => {
  // console.info("- sendErrMail", { subject, content });

  subject = `${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} Socket ${subject || ""} (${process.env.REACT_APP_SVR_NAME})`;
  from = from || process.env.REACT_APP_MAIL_FROM_ADDR;

  if (send && process.env.REACT_APP_MODE !== "LOCAL" && content && content.length > 3 && !mailSendDelay) {
    // if (send && content && content.length > 3 && !mailSendDelay) {
    if (lastContent !== content) {
      lastContent = _.cloneDeep(content);

      content += "<br/><br/><br/>------------ Stack ------------<br/><font color='#bbb'" + lastLogData.join("<br/>") + "</font><br/>" + content;

      mailSendDelay = true;

      // Api 서버 거치지 않고 직접 발송
      send({ to: process.env.REACT_APP_MAIL_ERR_TO_ADDR, from, subject, content });

      setTimeout(() => {
        mailSendDelay = false;
      }, 10 * 1000);
    }
  }
};

app.get("/", function (req, res) {
  res.status(200).json({
    common: {
      success: true,
      message: "welcome socket server!",
      error: null,
    },
  });
});

/**
 * Make node.js not exit on error
 */
process.on("uncaughtException", function (err) {
  console.info(err);
  console.error(err);
});

/** Console Log Format */
console.logCopy = console.log.bind(console);

console.log = function (...args) {
  let data = _.map(args, (arg) => (typeof arg === "object" ? JSON.stringify(arg) : arg)).join(" ");

  data = moment().format("YYYY-MM-DD HH:mm:ss") + " " + data;
  this.infoCopy(data);

  if (_.trim(data)) {
    data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.
    data = data.replace(/(\[\d{1,2}m)/gi, ""); // chalk ascii 값 삭제.  ([1m, [32m...)

    if (isWriteLog) {
      try {
        logger.debug(data);
      } catch (e) {}
    }

    data = data.replace(/\n/g, "<br/>");
    addLastLog(data);
  }
};

console.infoCopy = console.info.bind(console);

console.info = function (...args) {
  let data = _.map(args, (arg) => (typeof arg === "object" ? JSON.stringify(arg) : arg)).join(" ");

  data = moment().format("YYYY-MM-DD HH:mm:ss") + " " + data;
  this.infoCopy(data);

  if (_.trim(data)) {
    data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.
    data = data.replace(/(\[\d{1,2}m)/gi, ""); // chalk ascii 값 삭제.  ([1m, [32m...)

    try {
      logger.info(data);
    } catch (e) {}

    data = data.replace(/\n/g, "<br/>");
    addLastLog(data);
  }
};

console.warn = function (...args) {
  if (args && !_.isEmpty(args[0])) {
    let data = _.map(args, (arg) => (typeof arg === "object" ? JSON.stringify(arg) : arg)).join("<br/>");

    if (_.trim(data)) {
      data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.

      try {
        logger.warn(data); // 오류로그
      } catch (e) {}

      var trace = stackTrace.get();

      let file = trace[1].getFileName().replace(__dirname, "");
      let line = trace[1].getLineNumber() + ":" + trace[1].getColumnNumber();

      console.log("-> warn file", { file, line }, args);
    }
  }
};

console.error = function (...args) {
  if (args && !_.isEmpty(args[0])) {
    let data = _.map(args, (arg) => (typeof arg === "object" ? JSON.stringify(arg) : arg)).join("<br/>");

    if (_.trim(data)) {
      data = data.replace(/.token":([\'\"][^\'\"]+[\'\"])/g, `"token":"........"`); // 토큰정보 제거.

      try {
        logger.error(data);
      } catch (e) {}

      data = data.replace(/\s\[\d{2}m/g, "").replace(/\n/g, "<br/>"); // chalk ascii 값 삭제.  ([90m, [32m...)
      data = moment().format("YYYY-MM-DD HH:mm:ss") + "<br/><br/>" + data;

      var trace = stackTrace.get();

      let file = trace[1].getFileName().replace(__dirname, "");
      let line = trace[1].getLineNumber() + ":" + trace[1].getColumnNumber();

      console.log("-> error file", { file, line }, args);

      data += `<br/><br/>${file} ${line}`;

      sendErrMail("error@icrew.kr", "Error", data);
    }
  }
};

const cTable = require("console.table");

console.cTable = console.table.bind(console);

console.table = function (title, data) {
  if (title && data) this.logCopy(title);
  data = data || title;
  if (data) {
    const table = cTable.getTable([data]);
    console.log(chalk.hex("#9f7462")(table));
  }
};

// Device 소켓 서버 실행
const CcuServer = require("./socket/ccu/server");
global.CcuServer = new CcuServer();

// WEB 소켓 서버 실행
const WebServer = require("./socket/web/server");
global.WebServer = new WebServer();

// ADMIN 소켓 서버 실행
const AdmServer = require("./socket/admin/server");
global.AdmServer = new AdmServer();

// MOBILE 소켓 서버 실행
const MobileServer = require("./socket/mobile/server");
global.MobileServer = new MobileServer();

// PMS 소켓 서버 실행
const PmsServer = require("./socket/pms/server");
global.PmsServer = new PmsServer();
