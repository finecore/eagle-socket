const jwt = require("jsonwebtoken");
const { ERROR } = require("../constants/constants");

// JWT Secreet key.
const JWT_SECRET = "Eagle@$Jwt!#Secreet%^Key&";
const JWT_EXPIRE = 60 * 60 * 24 * 365; // 1년.

/**
 * JWT Utils.
 */
const verify = (token, callback) => {
  let log = "-----------  JWT VERIFY  -----------";

  if (!token) callback(ERROR.NO_CERTIFICATION, null);
  else {
    // jwt 에서 인증 정보 추출.
    jwt.verify(token, JWT_SECRET, (err, payload) => {
      if (err) console.log(log, "\n", "- jwt verify error ", err, payload);
      else console.log(log, "\n", "- jwt verify ok! ", payload, "----------------------------------");
      callback(err, payload);
    });
  }
};

const sign = (payload, callback, expires) => {
  // 유효 기간.
  var options = {
    expiresIn: expires || JWT_EXPIRE,
  };

  let log = "-----------  JWT SIGN  -----------";
  // console.log("- jwt sign ", JSON.stringify(payload), JSON.stringify(options));
  const { channel, place_id } = payload;

  if (channel !== "admin") {
    if (!place_id) {
      console.info(log, "\n", "---- 필수 입력 정보 place_id 누락!! ", payload);
      callback(ERROR.REQUIRE_CERTIFICATION, null);
      return;
    }
  }

  jwt.sign(payload, JWT_SECRET, options, (err, token) => {
    if (err) console.log(log, "\n", "- jwt sign error ", err);
    else console.log(log, "\n", "- jwt sign ok! ", { payload }, "----------------------------------");
    callback(err, token);
  });
};

module.exports = { verify, sign };
