const { ERROR } = require("constants/constants");
const logger = require("../../config/logger");
const { verify, sign } = require("./jwt-util");
const chalk = require("chalk");
const _ = require("lodash");
const env = process.env.NODE_ENV || "development";

/**
 * API Utils.
 */

const _success = (data, message = "ok", auth = {}) => {
  let logdate = _.map(data, (v, k) => {
    if (Array.isArray(v) && v.length > 3) {
      let len = v.length;

      v = v.slice(0, 3);
      v.push(`.... more [${len - 3}] ...`);
    }
    return v;
  });

  logdate = JSON.stringify(logdate);

  if (logdate) console.log("--> api success body: ", logdate);

  return {
    /** reponse format */
    common: {
      success: true,
      message,
      error: null,
    },
    body: data,
    auth, // 서버용 데이터.
  };
};

const _failure = (err = ERROR.DEFAULT, message = "ERROR", auth = {}) => {
  logger.error("--> api error: " + chalk.red(JSON.stringify(err) + " " + JSON.stringify(message)));

  let error = _.cloneDeep(err);

  // 오류 상세는 3 줄만 내려보낸다.
  if (error.detail && error.detail instanceof String) {
    error.detail = error.detail
      .split("\n")
      .filter((v, k) => v.trim().length && k < 3)
      .join("<br/>");
  }

  let { sqlMessage, sql } = error;
  if (env === "development" && sqlMessage) error.detail = [sqlMessage, sql];

  return {
    common: {
      success: false,
      message,
      error,
    },
    body: { info: {} },
    auth, // 서버용 데이터.
  };
};

/** reponse json */
const jsonRes = (_req, _res, _err, _data, _message) => {
  if (_err) {
    let error = _.cloneDeep(_err);

    if (_data) error.detail += "\n" + (_data instanceof Object ? JSON.stringify(_data) : _data);

    if (_req) {
      if (_req.app.get("env") === "development" && error.message && error.message.indexOf("[" + _req.url + "]") === -1) error.message = "[" + _req.url + "] " + error.message;
    } else error.message = "[API Error] " + error.message;

    // Don't response error "Cannot set headers after they are sent to the client"
    if (error.code !== "ERR_HTTP_HEADERS_SENT" && error.code !== "PROTOCOL_CONNECTION_LOST") {
      return _res.json(_failure(error, "ERROR", _res.auth));
    }
    return;
  } else {
    return _res.json(_success(_data, _message, _res.auth));
  }
};

/** reponse send */
const sendRes = (_res, _err, _data) => {
  return _err ? _res.send(_failure(_err)) : _res.send(_success(_data));
};

module.exports = { jsonRes, sendRes };
