import chalk from "chalk";

/**
 * Socket Parser Class.
 */
class SocketParser {
  constructor() {}

  isJson(json) {
    return /^[\],:{}\s]*$/.test(json.toString());
  }

  toJson(json) {
    if (typeof json === "object") return json;

    try {
      return JSON.parse(json);
    } catch (err) {
      return {};
    }
  }

  /**
   * buffer to json.
   *
   * @param {*} buff
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  decodeToArray(buff, encoding = "utf8") {
    let str = buff.toString("utf8");
    str = str.replace(/"\\/gi, "").trim();

    let datas = str.split(/\}\{/);

    return datas;
  }

  /**
   * buffer to json.
   *
   * @param {*} buff
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  decode(buff, encoding = "utf8") {
    var str = buff.toString(encoding);
    str = str.replace(/"\\/gi, "").trim();

    let split = str.split(/\}\{/);

    let json = split[0] || str;

    if (split.length > 1) json += "}";

    try {
      return JSON.parse(json);
    } catch (e) {
      console.info("전문 형식이 맞지 않습니다.", e, json);
      // json = this.error(500, e.message, "전문 형식이 맞지 않습니다.", json);
    }
    return null;
  }

  /**
   * json to buffer.
   *
   * @param {*} json
   * @param {string} [encoding="base64"] (ascii, utf8, base64)
   * @returns
   * @memberof SocketParser
   */
  encode(json = "", encoding = "utf8") {
    if (typeof json === "object") json = JSON.stringify(json);
    try {
      json = json.replace(/"\\/gi, "");
    } catch (e) {}

    // return new Buffer.from(json, encoding); // 버퍼에 담으면 딜레이 전송되어 문제 발생.
    return json;
  }

  success(json) {
    return {
      common: {
        success: true,
        error: null,
      },
      body: json || "",
    };
  }

  error(code = 500, message = "", detail = "", json = "") {
    return {
      common: {
        success: false,
        error: {
          code: code || 500,
          message: message,
          detail: detail || "소켓 오류 입니다.",
        },
      },
      body: json || "",
    };
  }
}

module.exports = SocketParser;
