import SocketLog from "model/socket-log";
import RoomState from "model/room-state";
import hexToBinary from "hex-to-binary";

const helper = socket => {
  let pos = 0;
  let model = {};
  let binary = "";
  let hexdata = "";

  return {
    parse: bytes => {
      // do something with bindata
      hexdata = new Buffer.from(bytes, "ascii").toString("hex");
      console.log("-> hexdata :", hexdata);

      binary = hexToBinary(hexdata);
      console.log("-> binary :", binary);

      pos = 0;

      model.header = hexdata.slice(0, 7 * 2); // header 7 byte hex.

      model = {
        ver: hexdata.slice(pos, (pos += 1 * 2)), // 1 byte hex 통신 프로토콜 버젼 정보
        id: hexdata.slice(pos, (pos += 2 * 2)), // 2 byte hex 0x0 : 서버 아이디,  N   : CCU ID, 0xFFFF : 전체 정보 전송 ( 시간 정보 전송시 )
        /**
         * 0x11 : 상태 요청      ( 서버 ==>  CCU )
         * 0x11 : 상태 응답      ( 서버 <==  CCU )
         * 0x21 : 상태 제어      ( 서버 ==>  CCU )
         * 0x21 : 제어 응답      ( 서버 <==  CCU )
         * 0x99 : 전체 상태 정보 요청 ( 서버 ==>  CCU )
         * 0x99 : 전체 상태 정보 응답 ( 서버 <==  CCU )
         */
        type: hexdata.slice(pos, (pos += 1 * 2)), // 1 byte hex
        len: hexdata.slice(pos, (pos += 2 * 2)), // 2 byte hex 1 ~ 255 : DATA 영역의 길이
        rid: hexdata.slice(pos, (pos += 1 * 2)) // 1 byte hex 객실 아이디 ( 1 ~ 255 )
      };

      model.data = [];

      // n byte hex 객실 정보
      for (var i = 0; i < 5; i++) {
        if (i < 2) model.data[i] = hexdata.slice(pos, (pos += 1 * 2));
        else model.data[i] = hexToBinary(hexdata.slice(pos, (pos += 1 * 2)));
        if (pos >= model.len) break;
      }

      model.crc = hexdata.slice(pos, (pos = +2 * 2)); // 2 byte hex 표준 CRC16 을 이용한 체크섬

      console.log("pos", pos, hexdata.length);
      console.log("parse model", model);

      return model;
    },
    putLog: (type, callback) => {
      let socketLog = {
        type: type || "0", // 전송 방향 (0: 수신, 1: 송신)
        ip: socket.remoteAddress,
        port: socket.remotePort,
        data: hexdata
      };

      SocketLog.putSocketLog(socketLog, (err, rows) => {
        callback(err, rows);
      });
    },
    putState: callback => {
      // ccu_id , rid 로 room_id 조회.
      RoomState.getRoomId([model.id, model.rid], (err, rows) => {
        if (err || !rows[0]) callback(err, rows);
        else {
          var roomState = {
            room_id: rows[0].id, // 객식 id
            type: model.data[0], // 유형 (01: 객실 상태, 02: 난방기 , 03: 조명 , 04: 딤머 , 05: 커튼 , 06: 보일러 )
            len: model.data[1], // 길이 hex
            byte_1: model.data[2],
            byte_2: model.data[3],
            byte_3: model.data[4],
            byte_4: model.data[5],
            mod_date: new Date()
          };

          console.log("parse roomState", roomState);

          // INSERT INTO ON DUPLICATE KEY UPDATE
          RoomState.setRoomState(roomState, (err, rows) => {
            callback(err, rows);
          });
        }
      });
    }
  };
};

export default helper;
