import request from "request";
import chalk from "chalk";
import _ from "lodash";

// .env config.
require("dotenv").config();

/**
 * Socket Helper Class.
 */
class SocketHelper {
  constructor() {
    this.host = process.env.REACT_APP_API_SERVER_HOST;
    this.port = process.env.REACT_APP_API_SERVER_PORT;
    this.api = this.host + ":" + this.port;
  }

  // route handle.
  route(json, callback) {
    if (!json || !json.headers || !json.headers.method) {
      callback(null, json);
    } else {
      const {
        headers: { method, url, token, serialno, channel = "web", uuid },
        body = {},
      } = json;

      // room state json object -> stringify
      if (url === "room/state" && body.state) {
        body.state = JSON.stringify(body.state);
      }

      const req = {
        url: this.api + "/" + url,
        method,
        headers: { channel, "x-access-token": token, token, serialno, uuid },
        body,
        json: true,
      };

      console.log(chalk.yellow(`---------< To API Server >---------`));
      console.log(chalk.grey(JSON.stringify(req)));
      console.log(chalk.yellow("-----------------------------------\n"));

      request(req, (err, res, data) => {
        console.log(chalk.yellow(`--------> From API Server <--------`));
        if (err) console.info(err, req, res);
        else {
          let { common, body } = data;

          if (body) {
            body = _.map(body, (v, k) => {
              // console.log(k, Array.isArray(v));

              if (Array.isArray(v) && v.length > 3) {
                let len = v.length;
                v = v.slice(0, 3);
                v.push(`.... more [${len - 3}] ...`);
              }

              return v;
            });
          }

          console.log(req.url, { common }, chalk.grey(JSON.stringify(body)));
        }
        console.log(chalk.yellow("-----------------------------------\n"));

        callback(err, data);
      });
    }
  }
}

module.exports = SocketHelper;
