const http = require("http");
const WebSocket = require("ws");
const uuidv4 = require("uuid/v4");

const chalk = require("chalk");
const _ = require("lodash");
const moment = require("moment");
const ip = require("ip");

// helder
const SocketHelper = require("helper/socket-helper");
const SocketParser = require("helper/socket-parser");
const apiUtil = require("utils/api-util");
const { verify } = require("utils/jwt-util");

// .env config.
require("dotenv").config();

// Admin <-> Admin API Server 소켓.
class AdmServer {
  constructor() {
    this.host = "0.0.0.0";
    this.ip = ip.address();
    this.port = process.env.REACT_APP_ADM_SOCKET_PORT;

    console.log("-> Admin AdmServer port : " + this.port);

    this.hostName = `${this.ip}:${this.port}`;

    // client ws list.
    this.clients = [];

    this.helper = new SocketHelper();
    this.parser = new SocketParser();

    this.init();

    setInterval(() => {
      if (this.clients.length) {
        _.each(this.clients, (socket) => {
          const { channel, place_id, user, uuid, user_place } = socket;
          console.log("- Admin socket check", { channel, place_id, user: user.id, uuid, user_place });
        });
      }
    }, 1000 * 60);
  }

  init() {
    /**
     * HTTP server
     */
    const server = http.createServer(function (request, response) {
      // Not important for us. We're writing AdminSocket server,
      // not HTTP server
    });

    server.listen(this.port, () => {
      console.log(chalk.yellow("--------- Admin Socket Server Start ----------"));
      var address = server.address();
      var port = address.port;
      var ipaddr = this.host;
      console.log("Server is listening at port : " + port);
      console.log("Server ip : " + ipaddr);
      console.log("-> Admin WEB Socket Server listening...");
      console.log(chalk.yellow("--------------------------------------------\n"));
    });

    server.on("close", () => {
      console.log("-> Admin AdmServer Socket onClose");
    });

    server.on("error", (error) => {
      console.info("-> Admin AdmServer Socket onError", error);
    });

    /**
     * WebSocket server
     */
    var wss = new WebSocket.Server({
      server,
    });

    wss.on("connection", (ws, req) => {
      ws.req = req;
      ws.isAlive = true;

      let clientName = `${req.connection.remoteAddress}:${req.connection.remotePort}`;

      console.log(chalk.yellow("----------- Admin Client connected -----------"));
      console.log("REMOTE Socket is listening  " + clientName);
      console.log(chalk.yellow("--------------------------------------------\n"));

      console.log("req " + req.session);

      // client append.
      ws.index = this.clients.push(ws) - 1;

      console.log("-- Admin client index ", ws.index);

      let clients = [];

      _.each(this.clients, (ws) => {
        if (ws && ws.readyState === WebSocket.OPEN) {
          clients.push(ws);
        }
      });

      this.clients = clients;

      // 고유 번호 부여.(브라우저에서 웹소켓 접속 시 마다 새로 생성)
      const uuid = uuidv4();

      // 데이터 수신.
      ws.on("message", (buff) => {
        let datas = String(buff);

        let logdate = _.map(data, (v, k) => {
          if (Array.isArray(v) && v.length > 3) {
            let len = v.length;

            v = v.slice(0, 3);
            v.push(`.... more [${len - 3}] ...`);
          }
          return v;
        });

        logdate = JSON.stringify(logdate);

        console.log(chalk.green(`--------> Admin From Client Browser <--------`));
        console.log(clientName + " -> Admin " + this.hostName);
        console.log(chalk.grey(logdate));
        console.log(chalk.green("-------------------------------------------\n"));

        // buffer to json.
        const json = this.parser.decode(buff, "utf8");

        // client 에서 변경 사항을 동일 업소의 사용자/장비로 전송.
        if (typeof json === "object" && json.type) {
          const { type, token, payload } = json;

          console.log("- client type", type);

          if (type === "JOIN_REQUESTED") {
            const { channel, user, user_place } = payload;

            ws.user = user;
            ws.channel = channel;
            ws.uuid = uuid;
            ws.user_place = user_place;

            // 응답
            const res = {
              type: "ws/uuid",
              payload: { uuid },
            };

            ws.send(JSON.stringify(res));

            console.log(chalk.yellow(`-----------< To Client Admin >------------`));
            console.log(this.hostName + " -> Admin " + clientName);
            console.log(chalk.grey(JSON.stringify(res)));
            console.log(chalk.yellow("----------------------------------------\n"));
          } else {
            // 각 단말 web 에 변경 사항 전파 한다.
            this.broadcast(json, ws);

            // 각 단말 device 에 변경 사항 전파 한다.
            global.CcuServer.broadcast(json, ws);
          }

          console.log("-> Admin user_place :" + ws.user_place.length, " ws.channel:", ws.channel, " uuid:", ws.uuid);
        } else {
          // json to buffer.
          const msg = this.parser.error(500, "전문 형식이 올바르지 않습니다.", "웹소켓 전송 메제지를 확인해 주세요");

          const _buff = this.parser.encode(msg, "utf8");

          // send to client.
          ws.send(_buff);

          console.log(chalk.yellow(`--------< To Client Admin Error >---------`));
          console.log(this.hostName + " -> Admin " + clientName);
          console.log(chalk.grey(JSON.stringify(msg)));
          console.log(chalk.yellow("----------------------------------------\n"));
        }
      });

      ws.on("pong", (buff) => {
        ws.isAlive = true;
      });

      ws.on("close", () => {
        console.log(`-> Admin Server connection from ${clientName} closed`);
      });

      ws.on("error", (err) => {
        console.log(`-> Admin Server  Connection ${clientName} error: ${err.message}`);
      });

      ws.on("timeout", () => {
        console.log("-> Admin Server timed out !");
        ws.end("Timed out!");
      });

      ws.on("end", (data) => {
        console.log("-> Admin Server End data : " + data);
      });
    });
  }

  getType({ type = "", url = "", message }) {
    if (type || url) {
      const urls = url.split("/");

      let { payload } = message;

      if (type === "SET_ISC_STATE" || url.indexOf("isc/state") === 0) {
        type = "iscState/item";
        if (!payload.isc_state.serialno && urls[2]) payload.isc_state.serialno = urls[2]; // serialno 추가.
      } else if (type === "SET_ISC_STATE_LOG" || url.indexOf("isc/state/log") === 0) {
        type = "iscStateLog/item";
      } else if (type === "SET_ISC_KEY_BOX" || url.indexOf("isc/key/box") === 0) {
        type = "iscKeyBox/item";
        if (!payload.isc_key_box.id && urls[3]) payload.isc_key_box.id = urls[3]; // id 추가.
      } else if (type === "SET_ISC_SET" || url.indexOf("isc/set") === 0) {
        type = "iscSet/item";
        if (!payload.isc_set.id && urls[2]) payload.isc_set.id = urls[2]; // id 추가.
      } else if (type === "SET_ROOM_STATE" || url.indexOf("room/state") === 0) {
        type = "roomState/item";
      } else {
        type = "";
      }
    }

    return type;
  }

  // Send a message to device
  // Admin -> Admin Admin & WEB -> Admin Admin 데이터 전송 용도 이다.
  // Admin -> Admin Admin : 분산 서버 환경일땐 로드 밸런서 에서 Client IP 로 세션 연관성 설정 해서 동일 업소에 장비를 동일 서버에 연결 한다.
  // WEB -> Admin Admin : Socket client-web 으로 서버에서 수신한 데이터를 동일 업소 Admin 에 전송한다.
  broadcast(json, sender) {
    // If there are no wss, then don't broadcast any messages
    if (this.clients.length === 0) {
      console.log("\n");
      return;
    }

    console.log("\n\n");
    console.log("--------------------------------------------");
    console.log("------------- brodcast to admin clients ");
    console.log("--------------------------------------------");

    console.log("-> Admin clients ", this.clients.length);

    let msg = JSON.stringify(json);
    msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;

    console.log("-> Admin json :", chalk.grey(msg));

    let { type, token, payload, place_id, headers, body } = json;

    let { channel, token: _token, url, uuid } = headers || {};

    let message = {
      websocket: true,
      token: token || _token,
      type,
      payload: payload || body,
      metadata: {
        uuid,
        createdAt: Date.now(), // 현재 일시 추가.
      },
    };

    if (!message.token) {
      console.log("-> Admin token 값이 없습니다!", message);
      console.log("\n");
      return;
    }

    if (!place_id && headers.place_id) place_id = headers.place_id;
    if (!place_id && message.payload.place_id) place_id = message.payload.place_id;
    if (!place_id) place_id = 0; // 관리자용

    if (place_id === undefined) {
      console.log("-> Admin place_id 가 없습니다!", message);
      console.log("\n");
      return;
    }

    // admin vuex type 으로 변환.
    message.type = this.getType({ type, url, message });

    if (!message.type) {
      console.log("-> Admin message type 이 없습니다!");
      console.log("\n");
    }

    console.log("-> Admin message type", message.type);

    _.map(this.clients, (ws) => {
      if (ws.readyState === WebSocket.OPEN) {
        let { user, user_place, uuid } = ws;

        if (user) {
          let { type, level, place_id: _place_id, uuid: _uuid } = user;

          console.log("-> Admin receiver ", _place_id, type, level, _uuid);

          // 타입 (1: 본사, 2: 대리점, 3: 고객)
          // 권한 (0: 슈퍼 관리자, 1: 관리자, 2: 사용자, 9: 뷰어)
          if (type === 1 && level < 2) {
            console.log(chalk.green("====> Admin send to client admin", level), "\n");
            ws.send(JSON.stringify(message));
          } else {
            // 송/수신 이 다른 업소 일 경우 관리 업소인지 체크 한다.(본사/대리점)
            if (Number(place_id) !== Number(_place_id)) {
              let place = _.find(user_place, { place_id });

              if (place) {
                _place_id = place.place_id;
                console.log("-> Admin user_place ", _place_id);
              }
            }

            // 동일 업소만 업데이트 한다.(중요!! 다른 업소에 정보가 노출 되면 안됨!)
            if (Number(place_id) === Number(_place_id)) {
              console.log(chalk.green("====> Admin send to client place_id:", place_id), "\n");
              ws.send(JSON.stringify(message));
            } else {
              console.log(chalk.grey("====> Admin no match client place_id:", place_id), "\n");
            }
          }
        }
      }
    });

    return;
  }
}

module.exports = AdmServer;
