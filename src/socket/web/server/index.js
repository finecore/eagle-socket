const http = require("http");
const WebSocket = require("ws");
const uuidv4 = require("uuid/v4");

const chalk = require("chalk");
const _ = require("lodash");
const moment = require("moment");
const ip = require("ip");

// helder
const SocketHelper = require("helper/socket-helper");
const SocketParser = require("helper/socket-parser");
const apiUtil = require("utils/api-util");
const { verify } = require("utils/jwt-util");

// .env config.
require("dotenv").config();

// Web <-> Web API Server 소켓.
class WebServer {
  constructor() {
    this.host = "0.0.0.0";
    this.ip = ip.address();
    this.port = process.env.REACT_APP_WEB_SOCKET_PORT;

    console.log("-> Web WebServer port : " + this.port);

    this.hostName = `${this.ip}:${this.port}`;

    // client ws list.
    this.clients = [];
    this.pingTimer = null;
    this.pingInterval = 10 * 1000;

    this.helper = new SocketHelper();
    this.parser = new SocketParser();

    this.init();

    // 접속 로그 메일 발송 체크.
    setInterval(() => {
      if (this.clients.length) {
        _.each(this.clients, (socket) => {
          const { place_id, uuid, id, type, licence_type } = socket;
          console.log("- Web socket check", { place_id, uuid, id, type, licence_type });
        });
      }
    }, 1000 * 60);
  }

  init() {
    /**
     * HTTP server
     */
    const server = http.createServer(function (request, response) {
      // Not important for us. We're writing WebSocket server,
      // not HTTP server
    });

    server.listen(this.port, () => {
      console.log(chalk.yellow("--------- Web Socket Server Start ----------"));
      var address = server.address();
      var port = address.port;
      var ipaddr = this.host;
      console.log("Server is listening at port : " + port);
      console.log("Server ip : " + ipaddr);
      console.log("-> Web WEB Socket Server listening...");
      console.log(chalk.yellow("--------------------------------------------\n"));
    });

    server.on("close", () => {
      console.info("-> Web WebServer Socket onClose");
      if (this.pingTimer) clearInterval(this.pingTimer);
    });

    server.on("error", (error) => {
      console.info("-> Web WebServer Socket onError", error);
    });

    /**
     * WebSocket server
     */
    var wss = new WebSocket.Server({
      server,
    });

    wss.on("connection", (ws, req) => {
      ws.req = req;
      ws.isAlive = true;

      let clientName = `${req.connection.remoteAddress}:${req.connection.remotePort}`;

      console.log("\n\n");
      console.log(chalk.yellow("----------- WEB Client connected -----------"));
      console.log("REMOTE Socket is listening  " + clientName);
      console.log(chalk.yellow("--------------------------------------------\n"));

      // client append.
      ws.index = this.clients.push(ws) - 1;

      console.log("-- Web client index ", ws.index);

      let clients = [];

      _.each(this.clients, (ws) => {
        if (ws && ws.readyState === WebSocket.OPEN) {
          clients.push(ws);
        }
      });

      this.clients = clients;

      // 고유 번호 부여.(브라우저에서 웹소켓 접속 시 마다 새로 생성)
      const uuid = uuidv4();

      // 데이터 수신.
      ws.on("message", (buff) => {
        // buffer to json.
        const json = this.parser.decode(buff, "utf8");

        console.log(chalk.green(`--------> Web From Client Browser <--------`));
        console.log(clientName + " -> Web " + this.hostName);
        console.log(chalk.yellow(JSON.stringify(json)));
        console.log(chalk.green("-------------------------------------------\n"));

        // client 에서 변경 사항을 동일 업소의 사용자/장비로 전송.
        if (typeof json === "object" && json.type) {
          const { type, token, payload } = json;

          console.log("- client type", type);

          if (type === "JOIN_REQUESTED" || type === "LOGOUT_REQUESTED") {
            const {
              user: { place_id, id, type: _type, licence_yn, licence_type, admin },
            } = payload;

            console.log("- client ", { place_id, id, _type, licence_yn, licence_type, admin });

            // 라이선스 여부 (이이크루 관리자는 패스)
            let isValidLicence = licence_yn === "Y" || _type === 1 || admin === "Y";

            // 접속 라이선스 체크.
            let access_count = 0;

            if (type === "JOIN_REQUESTED") {
              // 아이크루 관리자가 아니라면 라이선스 검증.
              if (isValidLicence && _type > 1 && admin !== "Y") {
                let accessWs = {};

                // 이미 접속 여부.
                _.each(this.clients, (ws) => {
                  if (ws && ws.readyState === WebSocket.OPEN) {
                    if (ws.login && place_id === ws.place_id) {
                      console.log("- ws place_id: ", ws.place_id, "id:", ws.id, "type:", ws.type);

                      access_count++;

                      if (_.trim(id) === _.trim(ws.id) && ws.type > 1) accessWs = ws;
                    }
                  }
                });

                console.log("- accessWs place_id: ", ws.place_id, "id:", ws.id, "type:", ws.type);

                if (accessWs.id) {
                  // 라이선스 타입 (1:고정 접속, 2:유동 접속)
                  if (licence_type === 1) {
                    isValidLicence = false;
                  } else if (licence_type === 2) {
                    // 기존 접속 계정 로그아웃.
                    const res = {
                      type: "DUP_ACCESS_LOGOUT",
                      payload: { access_count, licence_yn, licence_type, uuid },
                    };
                    accessWs.send(JSON.stringify(res));
                  }
                }
              }
            }

            console.log("- client access count", access_count);
            console.log("- isValidLicence ", isValidLicence);

            // 응답
            const res = {
              type: isValidLicence ? "SET_UUID" : "DUP_ACCESS_FAILUE",
              payload: { access_count, licence_yn, licence_type, uuid },
            };

            ws.send(JSON.stringify(res));

            ws.place_id = type === "JOIN_REQUESTED" ? place_id : null;
            ws.uuid = uuid;
            ws.id = id;
            ws.type = _type;
            ws.licence_type = licence_type;
            ws.login = type === "JOIN_REQUESTED";

            console.log(chalk.yellow(`-----------< To Client Web >------------`));
            console.log(this.hostName + " -> Web " + clientName);
            console.log(chalk.grey(JSON.stringify(res)));
            console.log(chalk.yellow("----------------------------------------\n"));
          } else {
            if (!ws.place_id) {
              // jwt 에서 인증 정보 추출.
              verify(token, (err, value) => {
                if (err) {
                  const msg = this.parser.error(401, "token 정보가 올바르지 않습니다.", "token 을 재 발급 받으세요.");

                  const _buff = this.parser.encode(msg, "utf8");

                  // send to client.
                  ws.send(_buff);

                  console.info(
                    chalk.yellow(`--------< To Client Device Error >---------`),
                    this.hostName + " -> Web " + clientName,
                    chalk.grey(JSON.stringify(msg)),
                    chalk.yellow("-------------------------------------------\n")
                  );
                  return;
                } else {
                  const { place_id } = value;

                  if (place_id) {
                    ws.place_id = place_id;

                    // 각 단말 web 에 변경 사항 전파 한다.
                    this.broadcast(json, ws);

                    // 각 단말 device 에 변경 사항 전파 한다.
                    global.CcuServer.broadcast(json, ws);
                  }
                }
              });
            } else {
              // 각 단말 web 에 변경 사항 전파 한다.
              this.broadcast(json, ws);

              // 각 단말 device 에 변경 사항 전파 한다.
              global.CcuServer.broadcast(json, ws);
            }
          }

          console.log("-> Web ws.place_id :" + ws.place_id, " uuid:", ws.uuid);
        } else {
          // json to buffer.
          const msg = this.parser.error(500, "전문 형식이 올바르지 않습니다.", "웹소켓 전송 메제지를 확인해 주세요");

          const _buff = this.parser.encode(msg, "utf8");

          // send to client.
          ws.send(_buff);

          console.info(
            chalk.yellow(`--------< To Client Web Error >---------`),
            this.hostName + " -> Web " + clientName,
            chalk.grey(JSON.stringify(msg)),
            chalk.yellow("----------------------------------------\n")
          );
        }
      });

      ws.on("pong", (buff) => {
        ws.isAlive = true;
        const { place_id, uuid } = ws;

        // console.log("-> Web Client receive a pong ", String(buff), place_id, uuid);
      });

      ws.on("close", () => {
        console.log(`-> Web Client connection from ${clientName} closed`);
      });

      ws.on("error", (err) => {
        console.log(`-> Web Server  Connection ${clientName} error: ${err.message}`);
      });

      ws.on("timeout", () => {
        console.log("-> Web Server timed out !");
        ws.end("Timed out!");
      });

      ws.on("end", (data) => {
        console.log("-> Web Server End data : " + data);
      });
    });
  }

  clientFormat({ type, token, url, body, uuid }) {
    // client store redux type.
    if (!type && url) {
      const urls = url.split("/");

      if (url.indexOf("user") === 0) type = "SET_USER";
      else if (url.indexOf("room/view/item") === 0) type = "SET_ROOM_VIEW_ITEM";
      else if (url.indexOf("room/view") === 0) type = "SET_ROOM_VIEW";
      else if (url.indexOf("room/state") === 0) {
        type = "SET_ROOM_STATE";
        if (body.room_state && urls[2]) body.room_state.room_id = Number(urls[2] || -1); // 객실 아이디 셋팅.
      } else if (url.indexOf("room/sale") === 0) {
        type = "SET_ROOM_SALE";
      } else if (url.indexOf("room/reserv/enable") === 0) {
        type = "SET_PLACE_ENABLE_RESERV_LIST";
      } else if (url.indexOf("room/interrupt") === 0) {
        type = "SET_ROOM_INTERRUPT";
      } else if (urls.length === 1 ? urls[0] === "room" : urls[0] === "room" && !isNaN(urls[1])) {
        type = "SET_ROOM";
      } else if (url.indexOf("isc/state") === 0) {
        type = "SET_ISC_STATE";
      } else if (url.indexOf("app/push") === 0) {
        type = "SET_APP_PUSH";
      }
    }

    return {
      websocket: true,
      token,
      type,
      payload: body,
      metadata: {
        uuid,
        createdAt: Date.now(), // 현재 일시 추가.
      },
    };
  }

  // Send a message to device
  // Web -> Web Web & WEB -> Web Web 데이터 전송 용도 이다.
  // Web -> Web Web : 분산 서버 환경일땐 로드 밸런서 에서 Client IP 로 세션 연관성 설정 해서 동일 업소에 장비를 동일 서버에 연결 한다.
  // WEB -> Web Web : Socket client-web 으로 서버에서 수신한 데이터를 동일 업소 Web 에 전송한다.
  broadcast(json, sender) {
    // If there are no wss, then don't broadcast any messages
    if (this.clients.length === 0) {
      console.log("\n");
      return;
    }

    console.log("\n\n");
    console.log("--------------------------------------------");
    console.log("------------- brodcast to web clients ");
    console.log("--------------------------------------------");

    console.log("-> Web clients ", this.clients.length);

    let msg = JSON.stringify(json);
    msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;

    console.log("-> Web json :", chalk.grey(msg));

    let { type, token, payload, place_id, uuid, req_channel = "web" } = json;

    let message = {
      websocket: true,
      token,
      type,
      payload,
      metadata: {
        uuid,
        req_channel,
        createdAt: Date.now(), // 현재 일시 추가.
      },
    };

    // URL 형식 전문은 redux store 형식으로 변환 한다.
    if (!message.type || !message.token) {
      let {
        headers: { token, url, place_id: _place_id, uuid: _uuid, req_channel: _req_channel },
        body,
      } = json;

      if (!place_id) place_id = _place_id;
      if (!uuid) uuid = _uuid;
      if (_req_channel) req_channel = _req_channel;

      message = this.clientFormat({
        type,
        token,
        url,
        body,
        uuid,
      });
    }

    console.log("- ", { place_id, uuid, req_channel });

    if (!message.type) {
      console.log("-> Web message type 이 없습니다!");
      console.log("\n");
      return;
    }

    if (message.type === "SET_APP_PUSH") {
      // 앱 푸시 전송 안함.
      console.log("-> SET_APP_PUSH 전송 안함 \n");
      console.log("\n");
      return;
    }

    if (!message.token) {
      console.log("-> Web token 값이 없습니다!");
      console.log("\n");
      return;
    }

    if (!place_id && sender) place_id = sender.place_id;

    if (!place_id) {
      console.log("-> Web place_id 가 없습니다!");
      console.log("\n");
      return;
    }

    // console.log("");
    // console.log(moment().format("YYYY-MM-DD HH:mm:ss"), "\n", "-> Web sender place_id", place_id, message.type, "\n", "-> Web uuid", uuid, "\n", JSON.stringify(message.payload), "\n");

    this.clients.forEach((ws) => {
      if (ws.readyState === WebSocket.OPEN) {
        const { place_id: _place_id, uuid: _uuid, login } = ws;

        if (_place_id && login) {
          // console.log("-> Web ws place_id", _place_id, " uuid", { uuid, _uuid });

          // 동일 업소만 업데이트 한다.(중요!! 다른 업소에 정보가 노출 되면 안됨!)
          if (Number(place_id) === Number(_place_id)) {
            // 동일 업소의 다른 사용자만 업데이트 한다.
            // 전송 채널이 WEB 이 아니라면(device 등) uuid 에 상관없이 전파. ADD_ROOM_STATE_LOG_LIST 는 uuid 에 상관 없이 전파.
            if (req_channel !== "web" || !uuid || uuid !== _uuid || message.type === "ADD_ROOM_STATE_LOG_LIST") {
              console.log(chalk.green("====> Web send to client place_id:", _place_id, " uuid", _uuid));
              ws.send(JSON.stringify(message));
            }
          }
        }
      }
    });

    return;
  }
}

module.exports = WebServer;
