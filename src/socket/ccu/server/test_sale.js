/**
 * TCP 클라이언트
 */
var net = require("net");
const chalk = require("chalk");
const { sign } = require("../../../utils/jwt-util");
const _ = require("lodash");
const moment = require("moment");

const PORT = 9090;
const HOST = "0.0.0.0";
//const HOST = "34.80.2.239"; // test LB
//const HOST = "35.229.220.180"; // prod LB

class Client {
  constructor(port, address) {
    this.address = address || HOST;
    this.port = port || PORT;

    this.interrupt = false;
    this.reconnect = null;

    this.serialno = "00000002";

    this.json = {
      headers: {
        method: "post",
        url: "room/sale",
        channel: "device",
        serialno: this.serialno,
        token: "",
      },
      body: {
        room_sale: {
          room_id: 1,
          state: "A",
          stay_type: 2,
          channel: "isc",
          check_in: moment().format("YYYY-MM-DD HH:mm:ss"),
          check_in_exp: moment().format("YYYY-MM-DD HH:mm:ss"),
          check_out_exp: moment().add(2, "hour").format("YYYY-MM-DD HH:mm:ss"),
          default_fee: "25000",
          pay_card_amt: 0,
          pay_cash_amt: 35000,
          add_fee: "10000",
        },
      },
    };

    this.init();
  }

  init() {
    this.socket = new net.Socket();

    this.socket.connect(this.port, this.address);

    this.socket.on("connect", (sock) => {
      console.log(`-> Test Client connected to: ${this.address} :  ${this.port}`);
      this.interrupt = false;

      // JWT(Json Web Token) 생성.
      const payload = {
        channel: "device",
        id: 1,
        place_id: 1,
        type: "01",
        serialno: this.serialno,
      };

      sign(payload, (err, token) => {
        this.json.headers.token = token;

        this.send(this.json);
      });

      //this.send(this.json);
    });

    this.socket.on("data", (buff) => {
      const data = JSON.stringify(buff.toString("utf8"));
      const json = JSON.parse(data);

      this.interrupt = false;

      console.debug(chalk.yellow(`<<<<<<<<< ${HOST} 서버 수신 <<<<<<<<< `) + "\n" + chalk.grey(json) + "\n");

      if (json.toString().endsWith("exit")) {
        this.socket.destroy();
      }
    });

    this.socket.on("error", (err) => {
      console.error("-> Test Client err", err);
    });

    this.socket.on("close", () => {
      console.log("-> Test Client closed");

      this.socket.destroy();

      setTimeout(() => {
        console.log("-> Test Client try reconnect", this.socket.writable);
        if (!this.socket.writable) {
          this.init();
        }
      }, 1000);
    });
  }

  send(data) {
    data = JSON.stringify(data);

    var buff = new Buffer.from(data, "utf8");

    // send to client.
    this.socket.write(buff);
    console.debug(chalk.yellow(`>>>>>>>>> ${HOST} 서버 송신 >>>>>>>>>`) + "\n" + chalk.grey(data) + "\n");
  }
}

new Client();
