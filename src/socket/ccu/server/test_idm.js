/**
 * TCP 클라이언트
 */
var net = require("net");
const chalk = require("chalk");
const { sign } = require("../../../utils/jwt-util");
const _ = require("lodash");

const PORT = 9090;
let HOST = "0.0.0.0";
// let HOST = "34.80.2.239"; // test LB
// let HOST = "35.229.220.180"; // prod LB
// let HOST = "35.201.175.230"; // prod 2

class Client {
  constructor(port, address) {
    this.address = address || HOST;
    this.port = port || PORT;

    this.interrupt = false;
    this.reconnect = null;

    this.serialno = "00000001";
    this.type = "01";

    // JWT(Json Web Token) 생성.
    const payload = {
      channel: "device",
      id: 1,
      place_id: 1,
      name: "IDM",
      type: this.type,
      serialno: this.serialno,
    };

    sign(payload, (err, token) => {
      console.log("-- token", token);
      this.token = token;

      this.json = {
        headers: {
          method: "put",
          url: "room/state/10",
          channel: "device",
          serialno: this.serialno,
          token: this.token,
        },
        body: {
          room_state: {
            key: 0,
            signal: 0,
          },
        },
      };

      this.json = {
        headers: {
          method: "put",
          url: "room/state/1",
          channel: "device",
          token: this.token,
        },
        body: {
          room_state: {
            key: 1,
          },
        },
      };

      this.init();

      this.send(this.json);

      // this.run();
    });
  }

  init() {
    this.socket = new net.Socket();

    this.socket.connect(this.port, this.address);

    this.socket.on("connect", (sock) => {
      console.log(`-> Test Client connected to: ${this.address} :  ${this.port}`);
    });

    this.socket.on("data", (buff) => {
      const data = JSON.stringify(buff.toString("utf8"));
      const json = JSON.parse(data);

      this.interrupt = false;

      console.debug(chalk.yellow(`<<<<<<<<< ${HOST} 서버 수신 <<<<<<<<< `) + "\n" + chalk.grey(json) + "\n");

      if (json.toString().endsWith("exit")) {
        this.socket.destroy();
      }
    });

    this.socket.on("error", (err) => {
      console.error("-> Test Client err", err);
    });

    this.socket.on("close", () => {
      console.log("-> Test Client closed");

      this.socket.destroy();

      setTimeout(() => {
        console.log("-> Test Client try reconnect", this.socket.writable);
        if (!this.socket.writable) {
          this.init();
        }
      }, 1000);
    });
  }

  send(data) {
    data = JSON.stringify(data);

    var buff = new Buffer.from(data, "utf8");

    // send to client.
    this.socket.write(buff);
    console.debug(chalk.yellow(`>>>>>>>>> ${HOST} 서버 송신 >>>>>>>>>`) + "\n" + chalk.grey(data) + "\n");
  }

  run() {
    let inx = 0;
    let room_id = 1;

    let queue = [{ key: 0 }, { key: 1 }, { key: 0 }, { door: 1 }, { door: 0 }, { clean: 1 }, { key: 3 }, { key: 0 }, { key: 1 }];

    // let keys = [0, 1];

    // 객실 현재 상태
    let now_state = [];

    // Test loop...
    setInterval(() => {
      if (!this.socket || !this.socket.writable) {
        console.log("-> Test Socket un writable");
      } else {
        if (!this.interrupt) {
          this.interrupt = true;

          console.log("-> queue", queue.length, inx);

          if (queue.length <= inx) {
            inx = 0;
            // return;
          }

          let room_state = queue[inx];

          // 랜덤값.
          const ran = Math.floor(Math.random() * 6);

          if (ran % 2 == 0) room_state.key = Math.floor(Math.random() * 4);
          if (ran % 3 == 0) room_state.door = Math.floor(Math.random() * 2);
          if (ran % 4 == 0) room_state.clean = Math.floor(Math.random() * 2);
          // if (ran % 5 == 0) room_state.air_power = Math.floor(Math.random() * 2);

          // if (ran % 6 == 0)
          //   room_state.light =
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2);

          //room_state = { clean: 1, key: 1 };

          room_state.signal = 0;

          console.log("-->", room_state);

          // randomData = Object.assign({}, randomData, room_state);

          if (room_id > 5) {
            room_id = 1;

            // return;
          }

          this.json.headers.url = "room/state/" + room_id;

          this.json.body.room_state = room_state;

          let clone = Object.assign({}, this.json);

          this.send(clone);

          inx++;
          room_id++;
        }
      }
    }, 3 * 1000);
  }
}

new Client();
