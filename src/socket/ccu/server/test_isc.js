/**
 * TCP 클라이언트
 */
var net = require("net");
const chalk = require("chalk");
const { sign } = require("../../../utils/jwt-util");
const _ = require("lodash");

const PORT = 9090;
let HOST = "0.0.0.0";
// let HOST = "34.80.2.239"; // test LB
// let HOST = "35.229.220.180"; // prod LB
// let HOST = "35.201.175.230"; // prod 2

class Client {
  constructor(port, address) {
    this.address = address || HOST;
    this.port = port || PORT;

    this.interrupt = false;
    this.reconnect = null;

    this.serialno = "00000001";
    this.type = "01";

    // JWT(Json Web Token) 생성.
    const payload = {
      channel: "device",
      id: 1,
      place_id: 1,
      name: "IDM",
      type: this.type,
      serialno: this.serialno,
    };

    sign(payload, (err, token) => {
      console.log("-- token", token);
      this.token = token;

      this.json = {
        headers: {
          method: "put",
          url: "room/state/10",
          channel: "device",
          serialno: this.serialno,
          token: this.token,
        },
        body: {
          room_state: {
            key: 0,
            signal: 0,
          },
        },
      };

      this.json = {
        headers: {
          method: "put",
          url: "isc/state/" + this.serialno,
          channel: "device",
          token: this.token,
        },
        body: {
          isc_state: {
            state: 0,
            /* 스텝
            1 : 판매대기 단계
            2 : 성인인증 모드
            3 : 예약 확인 단계
            4 : 객실 선택 단계 (예약 시 스킵)
            5 : 객실 선택 확인 단계 (예약 시 스킵)
            6 : 결제 단계 (예약 시 스킵)
            7 : 결제 확인 단계 및 카드키 토출
            8 : 영수증 출력 단계 (고객 선택 시) */
            step: 1,
            reserv_num: "123456",
            minor_mode: 1,
            language: 1,
            crd_state: 1, // 0: 대기상태 , 1 : 결제중  2 : 결제 완료 3 : 결제 실패 4 : 카드제거
            prt_state: 0, // 0 : 미출력  1 : 출력  (초기화시점 : 결제완료시 )
            dsp_id: 2, // 1 : 왼쪽 지폐방출기 , 2 : 오른쪽 지폐 방출기
            dsp_state: 0, // 0: 대기상태 , 1 : 방출동작중
            act_state: 0, // 0: 입수 금지상태 1: 입수 대기상태
            act_input_unit: 5000, //입수된 권종
            voice_call: 0,
          },
        },
      };

      this.init();

      this.send(this.json);

      // this.run();
    });
  }

  init() {
    this.socket = new net.Socket();

    this.socket.connect(this.port, this.address);

    this.socket.on("connect", (sock) => {
      console.log(`-> Test Client connected to: ${this.address} :  ${this.port}`);
    });

    this.socket.on("data", (buff) => {
      const data = JSON.stringify(buff.toString("utf8"));
      const json = JSON.parse(data);

      this.interrupt = false;

      console.debug(chalk.yellow(`<<<<<<<<< ${HOST} 서버 수신 <<<<<<<<< `) + "\n" + chalk.grey(json) + "\n");

      if (json.toString().endsWith("exit")) {
        this.socket.destroy();
      }
    });

    this.socket.on("error", (err) => {
      console.error("-> Test Client err", err);
    });

    this.socket.on("close", () => {
      console.log("-> Test Client closed");

      this.socket.destroy();

      setTimeout(() => {
        console.log("-> Test Client try reconnect", this.socket.writable);
        if (!this.socket.writable) {
          this.init();
        }
      }, 1000);
    });
  }

  send(data) {
    data = JSON.stringify(data);

    var buff = new Buffer.from(data, "utf8");

    // send to client.
    this.socket.write(buff);
    console.debug(chalk.yellow(`>>>>>>>>> ${HOST} 서버 송신 >>>>>>>>>`) + "\n" + chalk.grey(data) + "\n");
  }

  run() {
    let inx = 0;
    let room_id = 1;

    let queue = [{ key: 0 }, { key: 1 }, { key: 0 }, { door: 1 }, { door: 0 }, { clean: 1 }, { key: 3 }, { key: 0 }, { key: 1 }];

    // let keys = [0, 1];

    // 객실 현재 상태
    let now_state = [];

    // Test loop...
    setInterval(() => {
      if (!this.socket || !this.socket.writable) {
        console.log("-> Test Socket un writable");
      } else {
        if (!this.interrupt) {
          this.interrupt = true;

          console.log("-> queue", queue.length, inx);

          if (queue.length <= inx) {
            inx = 0;
            // return;
          }

          let room_state = queue[inx];

          // 랜덤값.
          const ran = Math.floor(Math.random() * 6);

          if (ran % 2 == 0) room_state.key = Math.floor(Math.random() * 4);
          if (ran % 3 == 0) room_state.door = Math.floor(Math.random() * 2);
          if (ran % 4 == 0) room_state.clean = Math.floor(Math.random() * 2);
          // if (ran % 5 == 0) room_state.air_power = Math.floor(Math.random() * 2);

          // if (ran % 6 == 0)
          //   room_state.light =
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2) +
          //     "/" +
          //     Math.floor(Math.random() * 2);

          //room_state = { clean: 1, key: 1 };

          room_state.signal = 0;

          console.log("-->", room_state);

          // randomData = Object.assign({}, randomData, room_state);

          if (room_id > 5) {
            room_id = 1;

            // return;
          }

          this.json.headers.url = "room/state/" + room_id;

          this.json.body.room_state = room_state;

          let clone = Object.assign({}, this.json);

          this.send(clone);

          inx++;
          room_id++;
        }
      }
    }, 3 * 1000);
  }
}

new Client();
