// load the Node.js TCP library
const net = require("net");
const chalk = require("chalk");
const _ = require("lodash");
const moment = require("moment");
const ip = require("ip");

// helder
const SocketHelper = require("helper/socket-helper");
const SocketParser = require("helper/socket-parser");
const apiUtil = require("utils/api-util");
const { send } = require("utils/mail-util");
const { verify, sign } = require("utils/jwt-util");

// .env config.
require("dotenv").config();

// Device <-> Tcp API Server 소켓.
class CcuServer {
  constructor() {
    this.host = "0.0.0.0";
    this.port = process.env.REACT_APP_TCP_SOCKET_PORT;
    this.ip = process.env.REACT_APP_MODE === "LOCAL" ? ip.address() : process.env.REACT_APP_EXT_IP; // 서버 외부 IP.

    console.log("-> Tcp CcuServer ip address : " + this.ip);

    this.hostName = `${this.ip}:${this.port}`;

    console.log("-> Tcp CcuServer hostName: " + this.hostName);

    this.isFirst = true;

    this.devices = [];
    this.clients = [];
    this.pingTimer = null;
    this.pingInterval = 60 * 1000; // 연결 확인 체크 시간.
    this.timeout = 60 * 10; // 타임아웃 시간(초)

    this.helper = new SocketHelper();
    this.parser = new SocketParser();

    this.connected = []; // 접속 로그.
    this.lsatConnectCnt = 0; // 1분전 접속 카운트.

    this.init();

    // 접속 로그 메일 발송 체크.
    setInterval(() => {
      // console.log("- lsatConnectCnt", this.lsatConnectCnt, "connected len", this.connected.length, changeBegin);

      // 1분 마다 실행.
      if (Number(moment().format("ss")) === 0) {
        let changeBegin = this.connected.length - this.lsatConnectCnt;

        // 1분 이내 100 개 이상 접속 이면..
        if (this.lsatConnectCnt > 100) {
          let data = _.map(this.connected.reverse(), (v, k) => {
            v = JSON.stringify(v);
            if (k > changeBegin) v = `<font color='#ccc'>${v}</font>`;
            return v;
          }).join("<br/>");

          console.info("socket connect log", { changeBegin }, data);

          // this.sendMail("info@icrew.kr", "connect log", "changeBegin: " + changeBegin + "<br/>" + data);
        }

        // 이전 접속 로그 삭제.
        if (this.lsatConnectCnt > 0) {
          this.connected.splice(0, changeBegin); // 이전로그 삭제.
          this.lsatConnectCnt = 0; // 로그 카운트 초기화.
        }

        if (this.clients.length) {
          _.each(this.clients, (socket) => {
            const { channel, place_id, serialno, duplicate } = socket;
            // console.log("- Tcp socket check", { channel, place_id, serialno, duplicate });
          });
        }
      }
    }, 1000);
  }

  sendMail(from, subject, content) {
    console.info("- sendMail", { subject });

    subject = `${process.env.REACT_APP_MODE !== "PROD" ? "[" + process.env.REACT_APP_MODE + "]" : ""} Socket ${subject || ""} (${process.env.REACT_APP_SVR_NAME})`;
    from = from || process.env.REACT_APP_MAIL_FROM_ADDR;

    // if (send && process.env.REACT_APP_MODE !== "LOCAL" && content && content.length > 3) {
    if (send && content && content.length > 3) {
      content = moment().format("YYYY-MM-DD HH:mm:ss") + "<br/><br/>" + content;

      // Api 서버 거치지 않고 직접 발송
      send({ to: process.env.REACT_APP_MAIL_ERR_TO_ADDR, from, subject, content });
    }
  }

  init() {
    const server = net.createServer((socket) => {
      let clientName = `${socket.remoteAddress}:${socket.remotePort}`;

      console.log(chalk.yellow("---------- Device Client connected ---------"));
      console.log("REMOTE Socket is listening  " + clientName);
      console.log(chalk.yellow("--------------------------------------------\n"));

      // client append.
      let index = this.clients.push(socket) - 1;

      console.log("-- Ccu client index ", index);

      socket.index = index;

      socket.isAlive = false; // 초기 수신 전까지 false.
      socket.device = null;
      socket.duplicate = null;

      // timeout 설정.(타임아웃은 제대로 동작 하지 않는다. 사용 안함)
      // socket.setTimeout(this.timeout);
      // keep-alive 설정
      socket.setKeepAlive(true, 10000); // ACK flag를 가진 빈 TCP Packet을 보내서 connection을 유지할 수 있다.
      // TCP Buffer의 전송 Delay 설정
      socket.setNoDelay(true); // write시 데이터를 즉시 보내게 할 수 있다.

      // 데이터 수신 큐.
      let queue = "";

      /*
        데몬 데이터 수신
        RCU 연결 문제 시 signal = 1 던짐. 정상 연결 시  signal = 0 던진다.
      */
      socket.on("data", (buff) => {
        // console.log("----> buff ", buff);

        // buffer to json.
        const datas = this.parser.decodeToArray(buff, "utf8");

        // console.log("----> datas ", datas);

        _.map(datas, (data, i) => {
          try {
            // console.log("----> a data ", i, queue, data);

            if (i === 0) {
              // 이전 마지막 끊긴 데이터가 있다면 붙여준다.
              if (queue) data = queue + data;
              queue = "";
            }

            if (datas.length > 1) {
              if (i === 0) data = data + "}";
              else if (i === datas.length - 1) data = "{" + data;
              else data = "{" + data + "}";
            }

            // console.log("----> data ", i, data);

            let json = JSON.parse(data);

            this.onData(socket, json);
          } catch (e) {
            // console.log(chalk.red(e + "\n" + data));
            // 전문 전송이 완료 되지 않았다면 다음 수신 데이터 앞에 붙여준다.
            if (i === datas.length - 1) queue += data;
          }
        });
      });

      socket.on("close", () => {
        console.log(chalk.red(`-> Tcp client connection from ${clientName} closed`));

        if (socket.serialno) {
          // 소켓 clients 에서 제거.
          _.remove(this.clients, {
            serialno: socket.serialno,
          });

          const data = {
            headers: {
              method: "put",
              url: "device/serialno/" + socket.serialno,
              channel: "device",
              token: socket.token,
            },
            body: {
              device: {
                connect: 1, // 0:정상, 1:비정상
              },
            },
          };

          let device = socket.device;

          // 네트웤 연결  정보를 API 서버에 전송 한다.
          this.helper.route(data, (err, res) => {
            if (!err && device) {
              device.connect = 1; // 0:정상, 1:비정상

              console.log(chalk.red("-> Tcp close device "), clientName, socket.serialno, device);

              // 소켓 메새지.
              let message = {
                type: "SET_DEVICE",
                headers: { url: "/", token: socket.token },
                body: {
                  device,
                },
                place_id: device.place_id, // 웹소켓 전송용 place_id
              };

              // 웹 에 단절된 기기 정보 전송 한다.
              global.WebServer.broadcast(message, socket);

              // 모바일 에 단절된 기기 정보 전송 한다.
              global.MobileServer.broadcast(message, socket);

              // 어드민에 전파 한다.
              global.AdmServer.broadcast(message, socket);

              // 연결 해제 정보 저장.
              let { place_id, name, serialno, server_ip, place_name } = device;
              this.connected.push({ type: "close", time: moment().format("YYYY-MM-DD HH:mm:ss"), place_id, name, serialno, server_ip, place_name });

              // 최대 접속 로그는 100 개.
              if (this.connected.length > 100) this.connected.shift();
            }
          });
        }

        // if (this.clients.length > 0 && socket.index > -1) this.clients.splice(socket.index, 1); // Remove client from socket array
      });

      socket.on("error", (err) => {
        console.log(`-> Tcp client  Connection ${clientName} error: ${err.message}`);
      });

      socket.on("timeout", () => {
        console.log("-> Tcp client timeout ! ", clientName);
      });

      socket.on("end", (data) => {
        console.log("-> Tcp client End data ", clientName, data);
      });
    });

    server.maxConnections = 2000;

    server.listen(this.port, this.host, () => {
      console.log(chalk.yellow("-------- Device Socket Server Start --------"));
      console.log("Server is listening at port : " + this.port);
      console.log("Server ip : " + this.ip);
      console.log("-> Tcp Device Socket Server listening...");
      console.log(chalk.yellow("--------------------------------------------\n"));

      this.token = null;

      this.makeToken((token) => {
        this.token = token;

        this.getDevices(token);

        if (this.pingTimer) clearInterval(this.pingTimer);

        // 주기적 장비 목록 조회.
        this.pingTimer = setInterval(() => {
          this.getDevices(token);
        }, this.pingInterval);
      });
    });

    server.on("close", () => {
      console.log(chalk.red("-> Tcp Server Socket onClose"));
    });

    server.on("error", (error) => {
      console.info(chalk.red("-> Tcp Server Socket onError"), error);
    });
  }

  onData(socket, json) {
    let clientName = `${socket.remoteAddress}:${socket.remotePort}`;

    const { headers, body, common } = json;

    console.log("\n\n");
    console.log(chalk.green(`--------> Tcp From Client ${headers && headers.channel ? headers.channel : "Device"} <--------`));
    console.log(clientName + " -> Tcp " + this.hostName);
    // let msg = JSON.stringify(json);
    // msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;
    console.log(chalk.grey(JSON.stringify(json)));
    console.log(chalk.green("----------------------------------------\n"));

    socket.update = new Date();

    if (!this.devices.length) {
      console.log(chalk.red("--->  devices 목록 조회 오류 입니다!"));
      return;
    }

    if (headers) {
      const {
        headers: {
          method,
          url,
          channel = "",
          token = "",
          place_id = "", // API 에서 전송 시 넘어온다.(from web)
          serialno = "", // API 에서 전송 시 넘어온다.(from web)
        },
        body,
      } = json;

      if (!channel) {
        const msg = this.parser.error(401, "channel 정보가 없습니다!", "소켓 전송 헤더에 channel 정보를 설정해 주세요.");

        this.sendError(socket, msg, null);

        return;
      }

      if (socket.duplicate && channel !== "api") {
        console.log(chalk.red("---> 중복 접속 으로 무효화된 소켓!"), socket.serialno);
        return;
      }

      socket.channel = channel;

      // 토큰 조회가 아니라면 토큰이 항상 있어야 한다.
      if (!token && url !== "auth/device/token") {
        const msg = this.parser.error(401, "token 값이 없습니다!", "소켓 전송 헤더에 token 정보를 설정해 주세요.");

        this.sendError(socket, msg, null);

        return;
      } else {
        if (token && (!socket.place_id || !socket.serialno)) {
          // jwt 에서 인증 정보 추출.
          verify(token, (err, value) => {
            if (err) {
              const msg = this.parser.error(401, "token 정보가 올바르지 않습니다.", "token 을 재 발급 받으세요.");

              this.sendError(socket, msg, null);
              return;
            } else {
              if (channel === "device") {
                const { place_id, type, serialno } = value;

                console.log("-> Tcp new socket : ", { place_id, type, serialno });

                if (!serialno || !place_id) {
                  const msg = this.parser.error(401, "token 정보가 올바르지 않습니다.", "serialno 가 없습니다.");

                  this.sendError(socket, msg, null);
                  return;
                }

                let device = _.find(this.devices, { serialno: serialno }); // 장비정보

                if (!device) {
                  const msg = this.parser.error(401, "serialno 가 올바르지 않습니다.", "[" + serialno + "] 로 등록된 장비가 없습니다.");

                  this.sendError(socket, msg, null);
                  return;
                }

                // 소켓에 장비 정보 매핑.
                this.setDevice(device);

                socket.place_id = place_id;
                socket.type = type; // 장비 타입 (01: 게이트 타입, 02: 홈 타입)

                // 동일한 일련번호로 접속한 장비가 있는지 확인.
                let soc = _.find(this.clients, { serialno: serialno });

                if (soc && soc !== socket) {
                  // 중복접속 설정.
                  soc.duplicate = true;
                  soc.place_id = null; // 이전 접속의 업소 아이디 제거(client 목록에서 제외)

                  this.setClient();

                  // json to buffer.
                  const msg = this.parser.error(501, "동일한 serialno [" + serialno + "] 로 이미 접속 되어 있습니다.", "현재 소켓으로 업데이트 됩니다.");

                  this.sendError(socket, msg, null);
                }

                // 소켓에 일련본호 추가.
                socket.serialno = serialno;
              }
            }
          });
        }

        socket.token = token;
      }

      // 장비에서 수신...
      if (channel === "device") {
        if (serialno) socket.serialno = serialno;

        let soc = _.find(this.clients, { serialno: socket.serialno }); // 소켓에 동일한 장비 있는지 여부.

        // 소켓 clients 에 없다면 추가 (가끔 socket 정보가 사라짐)
        if (!soc) {
          this.clients.push(socket);
          console.log("-> put socket client ", socket.serialno);
        }

        console.log("-> Tcp device socket.place_id : " + socket.place_id);
        console.log("-> Tcp device socket.serialno : " + socket.serialno);
        console.log("-> Tcp device socket.type : " + socket.type);

        // 처음 연결 또는 재 연결 시 device update.
        if ((!socket.isAlive || !socket.device || socket.device.connect === 1) && socket.token) {
          console.log("");
          console.log(chalk.yellow("-> Update Api to Device Connect!"), socket.device);
          console.log("");

          const data = {
            headers: {
              method: "put",
              url: "device/serialno/" + socket.serialno,
              channel: "device",
              token: socket.token,
            },
            body: {
              device: {
                connect: 0, // 0:정상, 1:비정상
                server_ip: this.ip, // 서버 IP
              },
            },
          };

          // device connect 정보를 API 서버에 전송 한다.
          this.helper.route(data, (err, res) => {
            let device = socket.device || _.find(this.devices, { serialno: socket.serialno });

            // 소켓 접속 여부.
            console.log(chalk.green("-----> Tcp socket on device"), device);

            if (!err && device) {
              socket.device = _.cloneDeep(device);

              socket.device.connect = 0;

              // 소켓 메새지.
              let message = {
                type: "SET_DEVICE",
                headers: { url: "/", token: socket.token },
                body: {
                  device: socket.device,
                },
                place_id: socket.place_id || socket.device.place_id, // 웹소켓 전송용 place_id
              };

              // 웹 에 접속된 기기 정보 전송 한다.
              global.WebServer.broadcast(message, socket);

              // 모바일 에 접속된 기기 정보 전송 한다.
              global.MobileServer.broadcast(message, socket);

              // 어드민에 전파 한다.
              message.type = "device/item";
              global.AdmServer.broadcast(message, socket);

              // 접속 소켓 정보 저장.
              let { place_id, name, serialno, server_ip, place_name } = device;

              this.connected.push({ type: "connect", time: moment().format("YYYY-MM-DD HH:mm:ss"), place_id, name, serialno, server_ip, place_name });
              console.log(chalk.green("-- connected log"), this.connected);

              // 최대 접속 로그는 100 개.
              if (this.connected.length > 100) this.connected.shift();
              // 접속 카운트 증가.
              this.lsatConnectCnt++;
            }

            socket.isAlive = true;
          });
        }

        // route.(db query)
        // 소켓으로 들어온 정보를 API 서버에 전송 한다.
        this.helper.route(json, (err, res) => {
          if (err) {
            // json to buffer.
            const msg = this.parser.error(err.code, "API 통신 오류 입니다.");

            this.sendError(socket, msg, JSON.stringify(json));
          } else {
            // json to buffer.
            const _buff = this.parser.encode(res, "utf8");

            console.log(chalk.green(`-------< Tcp API Response To Client Device >------`));
            console.log(this.hostName + " -> Tcp " + clientName);
            console.log(chalk.grey(this.helper.substr(res)));
            console.log(chalk.green("--------------------------------------------------\n"));

            // 처리 결과 send to client.
            socket.write(_buff + "\n");

            if (!err) {
              // DB 반영 된 것만 전송.
              if (res.common && res.common.success) {
                if (method.toLowerCase() !== "get") {
                  // 각 단말 device 에 변경 사항 전파 한다.
                  this.broadcast(json, socket);

                  // 웹 에 전파 한다.
                  global.WebServer.broadcast(json, socket);

                  // 모바일 에 전파 한다.
                  global.MobileServer.broadcast(json, socket);

                  // 어드민에 전파 한다.
                  global.AdmServer.broadcast(json, socket);
                } else {
                  if (url === "auth/device/token") {
                    const { device } = res.body;
                    socket.place_id = device.place_id;
                    socket.type = device.type; // 장비 타입 (01: 게이트 타입, 02: 홈 타입)
                    socket.serialno = device.serialno;
                  }
                }
              }
            }
          }
        });
      } else if (channel) {
        console.log("-> Tcp from API Server ", channel, method);

        if (method && method.toLowerCase() !== "get") {
          // 각 단말 device 에 변경 사항 전파 한다.
          this.broadcast(json, socket);

          // 웹 에 전파 한다.
          global.WebServer.broadcast(json, socket);

          // 모바일 에 전파 한다.
          global.MobileServer.broadcast(json, socket);

          // 어드민에 전파 한다.
          global.AdmServer.broadcast(json, socket);

          // PMS에 전파 한다.
          if (global.PmsServer) global.PmsServer.broadcast(json, socket);
        }
      } else {
        const msg = this.parser.error(501, "channel 정보가 올바르지 않습니다.", "채널 정보를 확인해 주세요.");

        this.sendError(socket, msg, null);

        return;
      }
    }
    // Device 응답 전문.
    else if (common) {
      const {
        common: { success, message },
      } = json;

      console.log(chalk.green(`-----< Tcp From Client Device Direct Response >-----`));
      console.log(this.hostName + " -> Tcp " + clientName);
      let msg = JSON.stringify(json);
      msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;
      console.log(chalk.grey(msg));
      console.log(chalk.green("----------------------------------------------------\n"));
    } else {
      // json to buffer.
      const msg = this.parser.error(500, "전문 형식이 올바르지 않습니다.", "웹소켓 전송 메제지를 확인해 주세요");

      this.sendError(socket, msg, null);
    }
  }

  setClient(isLoging) {
    let clients = [];

    // console.log("-> Tcp setClient socket clients", this.clients.length);

    // 소켓 접속 목록 체크.
    _.map(this.clients, (socket) => {
      if (socket) {
        const { channel, place_id, serialno, duplicate } = socket;

        if (socket.writable) {
          if (channel === "api" || (place_id !== undefined && serialno !== undefined)) {
            let soc = _.find(clients, { channel, serialno }); // 소켓에 동일한 장비 있는지 여부.

            if (!soc) {
              if (isLoging) {
                console.log("- Tcp push socket ", {
                  channel,
                  place_id,
                  serialno,
                  duplicate,
                });
              }

              clients.push(socket);
            }
          }
        } else {
          console.log(chalk.red("-> Tcp Server Socket un writable!"), {
            channel,
            place_id,
            serialno,
            duplicate,
          });
        }
      }
    });

    this.clients = clients;

    console.log("-> Tcp setClient alive cnt ", clients.length);
  }

  // device 정보 조회.
  getDevices(token) {
    this.setClient(true);

    const data = {
      headers: {
        method: "get",
        url: "device/all",
        channel: "device",
        token,
      },
      body: {},
    };

    this.helper.route(data, (err, res) => {
      if (!err && res) {
        const { common, body } = res;
        this.devices = body.devices;
        // console.log("-> Tcp get devices", body.devices);

        _.map(this.devices, (device) => {
          this.setDevice(device);
        });

        this.isFirst = false;
      }
    });
  }

  // 소켓에 device 정보 업데이트.
  setDevice(device) {
    if (!device) {
      console.info("-> Tcp no device");
      return;
    }

    let { place_name, name, type, connect, place_id, serialno } = device || {};

    let socket = _.find(this.clients, { serialno: serialno });

    // 소켓 접속 여부.
    if (socket) {
      console.log(chalk.green("-----> Tcp device info put socket " + serialno));

      socket.device = _.cloneDeep(device);

      let _connect = -1; // 0:정상, 1:비정상
      let isTimeout = false;

      // 데이터 수신 시간이 길다면 타임아웃.
      if (socket.update) {
        var ms = moment().diff(socket.update);
        var sec = ms / 1000;

        console.log("- last_update", sec, "초");

        // 마지막 업데이트 시간이 일정시간 지났을때 연결 끊김으로.
        if (sec > this.timeout) {
          isTimeout = true;
        }
      } else if (!socket.writable) {
        isTimeout = true;
      }

      if (isTimeout) {
        if (connect === 0) _connect = 1; // 타임아웃.
      } else {
        if (connect === 1) _connect = 0; // 타임 아웃 해제.
      }

      // DB 상 연결 상태 이지만 소켓 서버 재 시작 시 연결 상태로 설정 한다.(웹 소켓에 전달 하기 위해)
      if (this.isFirst) _connect = 0;

      // console.log("- _connect", _connect, "\n");

      // 연결 변경 시.
      if (_connect > -1) {
        console.log(chalk.green(`-> Tcp device connect change ${connect} -> ${_connect}`), "\n");

        const data = {
          headers: {
            method: "put",
            url: "device/serialno/" + serialno,
            channel: "device",
            token: this.token,
          },
          body: {
            device: {
              connect: _connect,
              server_ip: this.ip, // 서버 IP
            },
          },
        };

        device.connect = _connect;

        // 네트웤 연결  정보를 API 서버에 전송 한다.
        this.helper.route(data, (err, res) => {
          if (!err) {
            // 소켓 메새지.
            let message = {
              type: "SET_DEVICE",
              headers: { url: "/", token: this.token },
              body: {
                device,
              },
              place_id, // 웹소켓 전송용 place_id
            };

            // 웹 에 단절된 기기 정보 전송 한다.
            global.WebServer.broadcast(message, socket);

            // 모바일 에 단절된 기기 정보 전송 한다.
            global.MobileServer.broadcast(message, socket);

            // 어드민에 전파 한다.
            message.type = "device/item";
            global.AdmServer.broadcast(message, socket);
          }
        });

        socket.isAlive = _connect === 0;
      }
    } else {
      // console.info("-> Tcp socket is null", device.serialno);
    }
  }

  makeToken(callback) {
    // JWT(Json Web Token) 생성.
    const payload = {
      channel: "device",
      id: 1,
      place_id: 1,
      type: "01",
      serialno: "00000001",
    };

    sign(payload, (err, token) => {
      if (err) console.error("-- makeToken error", err);
      callback(token);
    });
  }

  clientFormat({ type, token, payload }) {
    let url = "";

    // client store redux type.
    if (type === "SET_ROOM_SALE") {
      url = "room/sale";
    } else if (type === "SET_ROOM_STATE") {
      url = "room/state";
    } else if (type === "SET_ROOM_STATE_ALL") {
      url = "room/state/all";
    } else if (type === "SET_ROOM_INTERRUPT") {
      url = "room/interrupt";
    }

    return {
      headers: { url, token },
      body: payload,
    };
  }

  sendError(socket, msg, data) {
    if (socket && socket.writable) {
      const _buff = this.parser.encode(msg, "utf8");

      socket.setNoDelay(true); // write시 데이터를 즉시 보내게 할 수 있다.

      // send to client.(개행문자를 추가해 달라는 요청으로 추가(상상제작소 이용욱 책임))
      socket.write(_buff + "\n");

      let clientName = `${socket.remoteAddress}:${socket.remotePort}`;

      console.info(
        chalk.red(`--------< Tcp To Client Device Error >---------`),
        this.hostName,
        " -> Tcp ",
        clientName,
        chalk.grey(JSON.stringify(msg)),
        data ? data : "",
        chalk.red("-----------------------------------------------\n")
      );
    }
  }

  // Send a message to device
  // Device -> Tcp Device & WEB -> Tcp Device 데이터 전송 용도 이다.
  // Device -> Tcp Device : 분산 서버 환경일땐 로드 밸런서 에서 Client IP 로 세션 연관성 설정 해서 동일 업소에 장비를 동일 서버에 연결 한다.
  // WEB -> Tcp Device : Socket client-web 으로 서버에서 수신한 데이터를 동일 업소 Device 에 전송한다.
  broadcast(json, sender) {
    // If there are no sockets, then don't broadcast any messages
    if (this.clients.length === 0) {
      console.log("\n");
      return;
    }

    console.log("\n\n");
    console.log("--------------------------------------------");
    console.log("------------- brodcast to device clients ");
    console.log("--------------------------------------------");

    console.log("-> Tcp clients ", this.clients.length);

    let msg = JSON.stringify(json);
    msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;

    console.log("-> Tcp json :", msg);

    let { headers, body, place_id } = json;

    let message = {
      headers,
      body,
    };

    if (!message.headers || !message.headers.url || !message.headers.url === "/") {
      let { type, token, payload } = json;

      message = this.clientFormat({
        type,
        token,
        payload,
      });
    }

    if (!message.headers.url) {
      console.info(chalk.red("-> Tcp url 값이 없습니다!"), message);
      console.log("\n");
      return;
    }

    if (!message.headers.token) {
      console.info(chalk.red("-> Tcp token 값이 없습니다!"), message);
      console.log("\n");
      return;
    }

    // 전송 할 업소의 id 체크.
    if (message.headers.place_id) place_id = message.headers.place_id;

    if (!place_id && sender) place_id = sender.place_id;

    if (!place_id) {
      console.info("-> Tcp place_id 가 없습니다!", message);
      console.log("\n");
      return;
    }

    const { url } = message.headers;

    // let msgBody = JSON.stringify(message.body);
    // msgBody = msgBody.length > 1000 ? msgBody.substring(0, 1000) + " ....more..." : msgBody;

    // console.log("");
    // console.log(moment().format("YYYY-MM-DD HH:mm:ss"), "\n", "-> Tcp sender place_id ", place_id, url, "\n", msgBody, "\n");

    if (url.indexOf("room/roomStateLog") > -1) {
      // 상태 로그는 전송 안함.
      console.log("-> ADD_ROOM_STATE_LOG_LIST 전송 안함 \n");
      return;
    }

    // 소켓 접속 목록.
    this.clients.forEach((socket) => {
      const { place_id: _place_id, type: _type, index: _index, serialno: _serialno, device } = socket;

      // 자기 자신 제외.
      if (sender !== socket && socket.writable && _place_id) {
        let { place_name, name, type, connect } = device || {};

        // console.log("--> Tcp clients device ", place_name, name, _serialno, _type, connect);

        // ccu 에는 state 만 전송
        // 구분 (01: IDM, 02: ISG, 10:RPT)
        if (_type === "01" && !url.startsWith("room/state/") && !url.startsWith("room/sale/")) {
          console.log("-> Tcp IDM 에는 room/state/, room/sale/ 만 전송", url);
        } else if (_type === "02" && !url.startsWith("room") && !url.startsWith("preference") && !url.startsWith("device") && !url.startsWith("isc")) {
          console.log("-> Tcp ISG 에는 room/, preference, device, isc/ 관련 전문 만 전송", url);
        } else if (_type === "10" && !url.startsWith("room") && !url.startsWith("preference")) {
          console.log("-> Tcp PRT 에는 room/, preference 관련 전문 만 전송", url);
        } else {
          // 동일 업소 기기만 업데이트 한다.(중요!! 다른 업소에 정보가 노출 되면 안됨!)
          if (_place_id && Number(place_id) === Number(_place_id)) {
            console.log(
              chalk.green(
                "====> Tcp send to client place_id:",
                _place_id,
                " type",
                _type,
                " serialno",
                _serialno
                // JSON.stringify(message)
              )
            );
            // console.log("-> Tcp socket server send message ", message);
            socket.write(JSON.stringify(message));
          }
        }
      }
    });

    return;
  }
}

module.exports = CcuServer;
