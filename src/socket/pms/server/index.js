// load the Node.js PMS library
const net = require("net");
const chalk = require("chalk");
const _ = require("lodash");
const moment = require("moment");
const ip = require("ip");
const uuidv4 = require("uuid/v4");

// helder
const SocketHelper = require("helper/socket-helper");
const SocketParser = require("helper/socket-parser");
const apiUtil = require("utils/api-util");
const { send } = require("utils/mail-util");
const { verify, sign } = require("utils/jwt-util");

// .env config.
require("dotenv").config();

// Pms <-> Server 소켓.
class PmsServer {
  constructor() {
    this.host = "0.0.0.0";
    this.port = process.env.REACT_APP_PMS_SOCKET_PORT;
    this.ip = process.env.REACT_APP_MODE === "LOCAL" ? ip.address() : process.env.REACT_APP_EXT_IP; // 서버 외부 IP.

    console.log("-> PmsServer ip address : " + this.ip);

    this.hostName = `${this.ip}:${this.port}`;

    console.log("-> PmsServer hostName: " + this.hostName);

    this.isFirst = true;

    this.clients = [];

    this.helper = new SocketHelper();
    this.parser = new SocketParser();

    this.preferences = [];
    this.rooms = [];

    this.token = "";

    this.init();

    const payload = {
      channel: "socket",
      pms: "pms",
      place_id: 1,
      mode: process.env.REACT_APP_MODE,
    };

    sign(payload, (err, token) => {
      if (token) {
        this.token = token;
        this.getPreferences();
        this.getRooms();
      }
    });

    setTimeout(() => {
      this.interval();
    }, 5000);
  }

  getPreferences() {
    let json = {
      headers: {
        channel: "socket",
        method: "get",
        url: "preferences/all",
        token: this.token,
      },
    };

    this.helper.route(json, (err, res) => {
      let { all_preferences } = res.body;

      console.log(chalk.yellow("--> all_preferences"), all_preferences.length);

      if (all_preferences && all_preferences.length) {
        this.preferences = all_preferences;
      }
    });
  }

  getRooms() {
    let json = {
      headers: {
        channel: "socket",
        method: "get",
        url: "room/all",
        token: this.token,
      },
    };

    this.helper.route(json, (err, res) => {
      let { rooms } = res.body;

      console.log(chalk.yellow("--> rooms"), rooms.length);

      if (rooms && rooms.length) {
        this.rooms = rooms;
      }
    });
  }

  interval() {
    setInterval(() => {
      const mmss = Number(moment().format("mmss"));
      const ss = Number(moment().format("ss"));

      // 1분 마다 업소 환경 설정 정보 조회.
      if (ss === 0) {
        this.getPreferences();
        this.getRooms();

        if (this.clients.length) {
          _.each(this.clients, (socket) => {
            const { channel, place_id, place_name, uuid } = socket;
            console.log("- Pms socket ", { channel, place_id, place_name, uuid });
          });
        }
      }

      // PMS 객실 상태 모두 요청.
      if (mmss === 0 && process.env.REACT_APP_MODE !== "LOCAL") {
        console.log(chalk.green("- PMS 객실 상태 주기적 수신 동기화."), this.clients.length);

        // 마이크로닉 프로토콜(추후 여러 업체시 별도 프로토콜 분리 해야함)
        let message = "RR";

        message += moment().format("YYYYMMDDHHmmss");
        message += "M";
        message += "??????";

        // 각 필드의  DATA길이는 40byte이다. 위의 DATA이외의 공간(24~40)은 공백처리한다.
        message = message.padEnd(40, " ");

        console.log(chalk.red("-> message ", message));

        // 소켓 접속 목록.
        this.clients.forEach((socket) => {
          const { place_id } = socket;

          if (socket.writable && place_id) {
            console.log(chalk.green("====> pms request all state"), place_id);
            socket.write(String(message));
          }
        });
      }
    }, 1000);
  }

  init() {
    const server = net.createServer((socket) => {
      let clientName = `${socket.remoteAddress}:${socket.remotePort}`;

      console.log(chalk.yellow("---------- Pms Client connected ---------"));
      console.log("REMOTE Socket is listening  " + clientName);
      console.log(chalk.yellow("--------------------------------------------\n"));

      let clients = [];

      _.each(this.clients, (soc) => {
        if (soc && soc.writable) {
          clients.push(soc);
        }
      });

      clients = _.uniqBy(clients, "place_id");

      this.clients = clients;

      // client append.
      let index = this.clients.push(socket) - 1;

      console.log("-- Pms client index ", index);

      socket.index = index;

      // TCP Buffer의 전송 Delay 설정
      socket.setNoDelay(true); // write시 데이터를 즉시 보내게 할 수 있다.

      // 고유 번호 부여.
      socket.uuid = uuidv4();

      /*
         데이터 수신
      */
      socket.on("data", (buff) => {
        // buffer to data.
        const data = buff.toString("utf8");

        console.log("----> pms data ", data);

        if (data) {
          const parse = this.fromDataParse(data);

          console.log(chalk.green(`--------> PMS From Client Socket <--------`));
          console.log(clientName + " -> Pms " + this.hostName);
          console.log(chalk.yellow(JSON.stringify(parse)));
          console.log(chalk.green("-------------------------------------------\n"));

          const { pms_code, pms_datetime, pms_div, pms_room_nm, pms_key, pms_reserve } = parse;

          // TODO 마이크로닉 프로토콜(추후 여러 업체시 별도 프로토콜 분리 해야함)
          // 인증.
          if (pms_code === "SC") {
            let pms_socket_access_key = pms_reserve;

            console.log("- preferences list", this.preferences.length, pms_socket_access_key);

            // PMS 업체 로컬 PC 에서 접속 시 고유 KEY (소켓 접속 토큰 발행용)
            let preferences = _.find(this.preferences, { pms_socket_access_key });

            console.log("- preferences", preferences.pms);

            if (preferences) {
              // JWT(Json Web Token) 생성.
              const { channel = "socket", place_id, pms, place_name } = preferences;
              const payload = { channel, place_id, pms, mode: process.env.REACT_APP_MODE }; // mode 는 소켓 토큰 에서 API 검증 시 사용.

              sign(payload, function (err, token) {
                if (token) {
                  socket.place_id = place_id;
                  socket.pms = pms;
                  socket.token = token;
                  socket.place_name = place_name;
                  console.log(chalk.grey("- 인증 성공!"), payload);
                } else {
                  console.log(chalk.red("- 인증 싫패!!"), payload);
                }
              });
            }
          } else {
            let { token, place_id, uuid } = socket;

            if (pms_code === "RR" || pms_code === "AS") {
              if (!token || !place_id) {
                if (!token) console.log(chalk.red("- token 정보가 없습니다."));
                if (!place_id) console.log(chalk.red("- place_id 정보가 없습니다."));
                return false;
              }
            }

            // All data request
            if (pms_code === "RR") {
              let json = {
                headers: {
                  channel: "socket",
                  method: "get",
                  url: "room/state/all/sale/" + place_id,
                  token,
                },
              };

              this.helper.route(json, (err, res) => {
                if (res.common && res.common.success) {
                  let { all_room_states } = res.body;

                  console.log("-> all_room_states", all_room_states.length);

                  _.each(all_room_states, (room_state, inx) => {
                    let { place_id, state, name } = room_state;

                    if (place_id && socket && socket.writable) {
                      let message = this.toDataParse({ place_id, state, name });

                      _.delay(
                        function (socket, message) {
                          try {
                            socket.write(message);
                            console.log(chalk.green("-> send ", message));
                          } catch (e) {
                            console.log("- error", e);
                          }
                        },
                        inx * 10,
                        socket,
                        message
                      );
                    }
                  });
                }
              });
            }
            // Room Data
            else if (pms_code === "AS" || pms_code === "DS" || pms_code === "KS") {
              let room = _.find(this.rooms, (v) => {
                let name = v.name.replace(/[^\d]/g, "");
                // console.log("- room name", v.name, parseInt(name), parseInt(pms_room_nm));
                return place_id === v.place_id && parseInt(name) === parseInt(pms_room_nm);
              });

              if (room && pms_key) {
                let { id: room_id } = room;

                console.log("-> room", room.name, room_id);

                let room_state = this.pmsToApiState(pms_code, pms_key);

                console.log("-> room_state", room_state);

                if (room_id && !_.isEmpty(room_state)) {
                  let json = {
                    headers: {
                      channel: "socket",
                      method: "put",
                      url: "room/state/" + room_id,
                      token,
                      place_id,
                      uuid,
                    },
                    body: { room_state },
                  };

                  // 소켓으로 들어온 정보를 API 서버에 전송 한다.
                  this.helper.route(json, (err, res) => {
                    console.log("-> res", res);
                  });
                }
              }
            }
            // Line Check
            else if (pms_code === "CK") {
            }
            // Version information
            else if (pms_code === "VI") {
            }
          }
        }
      });

      socket.on("close", () => {
        console.log(chalk.red(`-> client connection from ${clientName} closed`));
      });

      socket.on("error", (err) => {
        console.log(`-> client  Connection ${clientName} error: ${err.message}`);
      });

      socket.on("timeout", () => {
        console.log("-> client timeout ! ", clientName);
      });

      socket.on("end", (data) => {
        console.log("-> client End data ", clientName, data);
      });
    });

    server.maxConnections = 2000;

    server.listen(this.port, this.host, () => {
      console.log(chalk.green("-------- Pms Socket Server Start --------"));
      console.log("Server is listening at port : " + this.port);
      console.log("Server ip : " + this.ip);
      console.log("-> Pms Socket Server listening...");
      console.log(chalk.green("--------------------------------------------\n"));
    });

    server.on("close", () => {
      console.log(chalk.red("-> Server Socket onClose"));
    });

    server.on("error", (error) => {
      console.info(chalk.red("-> Server Socket onError"), error);
    });
  }

  fromDataParse(data) {
    // console.log("- fromDataParse data", data);
    const match = _.trim(data).match(/(SC|RR|AS|DS|KS|CK|VI)(\d{14,14})(.{1,1})(.{6,6})(.{1,1})?(.*)/);

    console.log("-> pms fromDataParse match", match);

    return {
      pms_code: match[1],
      pms_datetime: match[2],
      pms_div: match[3],
      pms_room_nm: match[4],
      pms_key: match[5],
      pms_reserve: match[6],
    };
  }

  toDataParse({ place_id, state, name }) {
    // check in	CI
    // check out	CO
    // All data request	RR
    // Line check	CK

    console.log("-> pms toDataParse ", { place_id, name, state });

    let message = state === "A" ? "CI" : "CO";

    message += moment().format("YYYYMMDDHHmmss");
    message += "M";
    message += name.replace(/[^\d]/g, "").padStart(6, "0");

    // 각 필드의  DATA길이는 40byte이다. 위의 DATA이외의 공간(24~40)은 공백처리한다.
    message = message.padEnd(40, " ");

    console.log("-> pms message ", message);

    return message;
  }

  pmsToApiState(pms_code, pms_key) {
    let room_state = {};

    if (pms_code === "AS") {
      // 실제 주 사용은 이글로 처리.
      if (pms_key === "E") {
        room_state = { outing: 0 }; // Occupied: E (재실)
      } else if (pms_key === "T") {
        room_state = { outing: 1 }; // Absence: T (외출)
      } else if (pms_key === "V") {
        // room_state = { key: 4 }; // Vacant: V (체크인 안되고 키없는 상태) (키 상태는 아래 KS 코드에서 적용)
      } else if (pms_key === "G") {
        room_state = { clean: 1 }; // Dirty: G (청소 지시)
      } else if (pms_key === "C") {
        room_state = { clean: 1 }; // Cleaning: C (청소 중)
      } else if (pms_key === "K") {
        room_state = { clean: 0 }; //  Cleaned: K (청소 완료)
      } else if (pms_key === "I") {
        room_state = {}; //  inspected: I (점검완료, 실제 점검완료 상태이지만 이글에서는 대응되는 플래그가 없음)
      }
    } else if (pms_code === "DS") {
      if (pms_key === "G") {
        room_state = { door: 1 }; //  Door Open
      } else if (pms_key === "C") {
        room_state = { door: 0 }; //  Door Close
      }
    } else if (pms_code === "KS") {
      // 1: 손님키 삽입
      // 2: 마스터키 삽입
      // 3: 청소키 삽입
      // 4: 손님키 제거
      // 5: 마스터키 제거
      // 6: 청소키 제거
      let key = Number(pms_key);

      room_state = { key };
    }

    return room_state;
  }

  apiToPmsKey(room_state) {
    let pms_key = "";

    let { key, sale, clean } = room_state;

    if (key === 1) pms_key = "E"; // Occupied: E(재실 (체크인 아니라도) 키있는상태)
    if ((key === 0 || key === 4) && sale > 1) pms_key = "T"; // Absence: T(체크인 키없는상태)
    if ((key === 0 || key === 4) && sale === 0) pms_key = "V"; // Vacant:V(공실 (체크인 안되고 키없는 상태))
    if (clean === 1) pms_key = "G"; // Dirty: G(청소 지시)
    if (key === 3 && clean === 1) pms_key = "C"; // Cleaning: C(청소 중)
    if (key === 6 && clean === 0) pms_key = "K"; //  Cleaned: K(청소 완료)

    // console.log("-> apiToPmsKey ", { key, sale, clean }, "-->", { pms_key });

    return pms_key;
  }

  // Send a message to pms
  // Pms -> Pms & WEB -> Pms 데이터 전송 용도 이다.
  // Pms -> Pms : 분산 서버 환경일땐 로드 밸런서 에서 Client IP 로 세션 연관성 설정 해서 동일 업소에 장비를 동일 서버에 연결 한다.
  // WEB -> Pms : Socket client-web 으로 서버에서 수신한 데이터를 동일 업소 Pms 에 전송한다.
  broadcast(json, sender) {
    if (this.clients.length === 0) {
      // console.log("------------- brodcast to pms clients count 0");
      return;
    }

    console.log("\n\n");
    console.log("--------------------------------------------");
    console.log("------------- brodcast to pms clients ", this.clients.length);
    console.log("--------------------------------------------");

    let msg = JSON.stringify(json);
    msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;

    // console.log("-> json", json);
    console.log("-> pms json msg", msg);

    let {
      type,
      headers: { url, channel, token, place_id },
      uuid,
      body: { room_state, room_sale },
    } = json;

    // if (room_state) console.log(chalk.yellow("-> pms room_state"), room_state);
    // if (room_sale) console.log(chalk.yellow("-> pms room_sale"), room_sale);

    if (!room_state && !room_sale) {
      console.log("-> ROOM_STATE, ROOM_SALE 이외는 전송 안함 \n");
      return;
    }

    if (!token) {
      console.info(chalk.red("-> token 값이 없습니다!"));
      console.log("\n");
      return;
    }

    if (!place_id) {
      console.info("-> place_id 가 없습니다!");
      console.log("\n");
      return;
    }

    // 동일 업소 기기만 업데이트 한다.(중요!! 다른 업소에 정보가 노출 되면 안됨!)
    let place_socket = _.filter(this.clients, { place_id }) || [];

    console.log(chalk.red("-> pms place_socket", place_socket.length));

    if (place_socket.length) {
      let { room_id, sale, state } = room_state || room_sale;

      if (room_id && (sale !== undefined || state !== undefined)) {
        let room = _.find(this.rooms, { id: room_id });

        // console.log(chalk.yellow("-> pms room"), room);

        let { place_id, name } = room;

        if (place_id) {
          // sale 정보가 있으면 state 재설정.
          if (sale !== undefined) state = sale === 0 ? "C" : "A";

          let message = this.toDataParse({ place_id, state, name });

          // console.log(chalk.red("-> message", message));

          // 소켓 접속 목록.
          place_socket.forEach((socket) => {
            const { place_id, uuid: _uuid } = socket;

            // 자기 자신 제외.
            if (sender !== socket && socket.writable) {
              console.log(chalk.green("====> pms send to client place_id:", { place_id, message }));
              socket.write(String(message));
            }
          });
        }
      }
    }

    return;
  }
}

module.exports = PmsServer;
