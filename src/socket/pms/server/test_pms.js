/**
 * TCP 클라이언트
 */
var net = require("net");
const chalk = require("chalk");
const { sign } = require("../../../utils/jwt-util");
const _ = require("lodash");
const moment = require("moment");

const PORT = 9094;
let HOST = "0.0.0.0";
// let HOST = "35.236.165.117"; // test
// let HOST = "34.80.225.129"; // prod
// let HOST = "35.201.175.230"; // prod 2

class Client {
  constructor(port, address) {
    this.address = address || HOST;
    this.port = port || PORT;

    this.interrupt = false;
    this.reconnect = null;

    this.init();

    this.send(`SC${moment().format("YYYYMMDDHHmmss")}--------019-0729-Mic-Hxt`); // ICT
    // this.send(`SC${moment().format("YYYYMMDDHHmmss")}--------020-1221-Mic-Hxt`); // 인천게양-소울하다

    setTimeout(() => {
      // 도어 열림
      this.send(`DS${moment().format("YYYYMMDDHHmmss")}M000501G`);
      // this.send(`DS${moment().format("YYYYMMDDHHmmss")}M001103G`);
    }, 3000);

    setTimeout(() => {
      // 도어 닫힘
      this.send(`DS${moment().format("YYYYMMDDHHmmss")}M000501C`);
      // this.send(`DS${moment().format("YYYYMMDDHHmmss")}M001103C`);
    }, 6000);
  }

  init() {
    this.socket = new net.Socket();

    this.socket.connect(this.port, this.address);

    this.socket.on("connect", (sock) => {
      console.log(`-> Test Client connected to: ${this.address} :  ${this.port}`);
    });

    let cnt = 1;

    this.socket.on("data", (buff) => {
      const data = JSON.stringify(buff.toString("utf8"));
      const json = JSON.parse(data);

      this.interrupt = false;

      console.debug(chalk.yellow(`<<<<<<<<< ${HOST} 서버 수신 <<<<<<<<< `), cnt++, "\n", chalk.red(json) + "\n");

      if (json.toString().endsWith("exit")) {
        this.socket.destroy();
      }
    });

    this.socket.on("error", (err) => {
      console.error("-> Test Client err", err);
    });

    this.socket.on("close", () => {
      console.log("-> Test Client closed");

      this.socket.destroy();

      setTimeout(() => {
        console.log("-> Test Client try reconnect", this.socket.writable);
        if (!this.socket.writable) {
          this.init();
        }
      }, 1000);
    });
  }

  send(data) {
    var buff = new Buffer.from(data, "utf8");

    // send to client.
    this.socket.write(buff);
    console.debug(chalk.green(`>>>>>>>>> ${HOST} 서버 송신 >>>>>>>>>`) + "\n" + chalk.white(data) + "\n");
  }
}

new Client();
