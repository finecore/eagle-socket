const http = require("http");
const WebSocket = require("ws");
const uuidv4 = require("uuid/v4");

const chalk = require("chalk");
const _ = require("lodash");
const moment = require("moment");
const ip = require("ip");

// helder
const SocketHelper = require("helper/socket-helper");
const SocketParser = require("helper/socket-parser");
const apiUtil = require("utils/api-util");
const { verify } = require("utils/jwt-util");

// .env config.
require("dotenv").config();

// Mobile <-> Mobile API Server 소켓.
class MobileServer {
  constructor() {
    this.host = "0.0.0.0";
    this.ip = ip.address();
    this.port = process.env.REACT_APP_SMT_SOCKET_PORT;

    console.log("-> Mobile MobileServer port : " + this.port);

    this.hostName = `${this.ip}:${this.port}`;

    // client ws list.
    this.clients = [];

    this.helper = new SocketHelper();
    this.parser = new SocketParser();

    this.init();

    setInterval(() => {
      if (this.clients.length) {
        console.log("-> Mobile MobileServer port : " + this.port);

        _.each(this.clients, (socket) => {
          const { place_id, uuid } = socket;
          console.log("- Mobile socket check", { place_id, uuid });
        });
      }
    }, 1000 * 60);
  }

  init() {
    /**
     * HTTP server
     */
    const server = http.createServer(function (request, response) {
      // Not important for us. We're writing WebSocket server,
      // not HTTP server
    });

    server.listen(this.port, () => {
      console.log(chalk.yellow("--------- Mobile Socket Server Start ----------"));
      var address = server.address();
      var port = address.port;
      var ipaddr = this.host;
      console.log("Server is listening at port : " + port);
      console.log("Server ip : " + ipaddr);
      console.log("-> Mobile Mobile Socket Server listening...");
      console.log(chalk.yellow("--------------------------------------------\n"));
    });

    server.on("close", () => {
      console.info("-> Mobile MobileServer Socket onClose");
      if (this.pingTimer) clearInterval(this.pingTimer);
    });

    server.on("error", (error) => {
      console.info("-> Mobile MobileServer Socket onError", error);
    });

    /**
     * WebSocket server
     */
    var wss = new WebSocket.Server({
      server,
    });

    wss.on("connection", (ws, req) => {
      ws.req = req;
      ws.isAlive = true;

      let clientName = `${req.connection.remoteAddress}:${req.connection.remotePort}`;

      console.log(chalk.yellow("----------- Mobile Client connected -----------"));
      console.log("REMOTE Socket is listening  " + clientName);
      console.log(chalk.yellow("-----------------------------------------------\n"));

      console.log("req " + req.session);

      // client append.
      ws.index = this.clients.push(ws) - 1;

      console.log("-- Mobile client index ", ws.index);

      let clients = [];

      _.each(this.clients, (ws) => {
        if (ws && ws.readyState === WebSocket.OPEN) {
          clients.push(ws);
        }
      });

      this.clients = clients;

      // 고유 번호 부여.(브라우저에서 웹소켓 접속 시 마다 새로 생성)
      const uuid = uuidv4();

      // 데이터 수신.
      ws.on("message", (buff) => {
        console.log(chalk.green(`--------> Mobile From Client Browser <--------`));
        console.log(clientName + " -> Mobile " + this.hostName);
        console.log(chalk.grey(buff));
        console.log(chalk.green("----------------------------------------------\n"));

        // buffer to json.
        const json = this.parser.decode(buff, "utf8");

        // client 에서 변경 사항을 동일 업소의 사용자/장비로 전송.
        if (typeof json === "object" && json.type) {
          const { type, token, payload } = json;

          console.log("- client type", type);

          if (type === "JOIN_REQUESTED") {
            const {
              user: { place_id },
            } = payload;

            ws.place_id = place_id;
            ws.uuid = uuid;

            // 응답
            const res = {
              type: "authStore/setUuid",
              payload: { uuid },
            };

            ws.send(JSON.stringify(res));

            console.log(chalk.yellow(`-----------< To Client Mobile >------------`));
            console.log(this.hostName + " -> Mobile " + clientName);
            console.log(chalk.grey(JSON.stringify(res)));
            console.log(chalk.yellow("-------------------------------------------\n"));
          } else {
            if (!ws.place_id) {
              // jwt 에서 인증 정보 추출.
              verify(token, (err, value) => {
                if (err) {
                  const msg = this.parser.error(401, "token 정보가 올바르지 않습니다.", "token 을 재 발급 받으세요.");

                  const _buff = this.parser.encode(msg, "utf8");

                  // send to client.
                  ws.send(_buff);

                  console.info(
                    chalk.yellow(`--------< To Client Device Error >---------`),
                    this.hostName + " -> Mobile " + clientName,
                    chalk.grey(JSON.stringify(msg)),
                    chalk.yellow("-------------------------------------------\n")
                  );
                  return;
                } else {
                  const { place_id } = value;

                  if (place_id) {
                    ws.place_id = place_id;

                    // 각 단말 web 에 변경 사항 전파 한다.
                    this.broadcast(json, ws);

                    // 각 단말 device 에 변경 사항 전파 한다.
                    global.CcuServer.broadcast(json, ws);
                  }
                }
              });
            } else {
              // 각 단말 web 에 변경 사항 전파 한다.
              this.broadcast(json, ws);

              // 각 단말 device 에 변경 사항 전파 한다.
              global.CcuServer.broadcast(json, ws);
            }
          }

          console.log("-> Mobile ws.place_id :" + ws.place_id, " uuid:", ws.uuid);
        } else {
          // json to buffer.
          const msg = this.parser.error(500, "전문 형식이 올바르지 않습니다.", "웹소켓 전송 메제지를 확인해 주세요");

          const _buff = this.parser.encode(msg, "utf8");

          // send to client.
          ws.send(_buff);

          console.info(
            chalk.yellow(`--------< To Client Mobile Error >---------`),
            this.hostName + " -> Mobile " + clientName,
            chalk.grey(JSON.stringify(msg)),
            chalk.yellow("-------------------------------------------\n")
          );
        }
      });

      ws.on("pong", (buff) => {
        ws.isAlive = true;
        const { place_id, uuid } = ws;

        // console.log("-> Mobile Client receive a pong ", String(buff), place_id, uuid);
      });

      ws.on("close", () => {
        console.log(`-> Mobile Server connection from ${clientName} closed`);
      });

      ws.on("error", (err) => {
        console.log(`-> Mobile Server  Connection ${clientName} error: ${err.message}`);
      });

      ws.on("timeout", () => {
        console.log("-> Mobile Server timed out !");
        ws.end("Timed out!");
      });

      ws.on("end", (data) => {
        console.log("-> Mobile Server End data : " + data);
      });
    });
  }

  getType(url, payload) {
    let type = "";

    if (url) {
      const urls = url.split("/");

      if (url.indexOf("user") === 0) type = "authStore/setUser";
      else if (url.indexOf("room") === 0 && urls.length === 1) type = "roomStore/setItem";
      else if (url.indexOf("room/view/item") === 0) type = "roomViewItemStore/setItem";
      else if (url.indexOf("room/view") === 0) type = "roomViewStore/setItem";
      else if (url.indexOf("room/state") === 0) {
        type = "roomStateStore/setItem";
        if (payload.room_state && urls[2]) payload.room_state.room_id = Number(urls[2] || -1); // 객실 아이디 셋팅.
      } else if (url.indexOf("room/sale") === 0) {
        type = "roomSaleStore/setItem";
      } else if (url.indexOf("room/roomStateLog") === 0) {
        type = "roomStateLogStore/addList";
      } else if (url.indexOf("room/reserv/enable") === 0) {
        type = "roomReservStore/setList";
      } else if (url.indexOf("room/interrupt") === 0) {
        type = "roomInterruptStore/setItem";
      } else if (url.indexOf("isc/key/box") === 0) {
        type = "iscKeyBoxStore/setItem";
        if (!payload.isc_key_box.id && urls[3]) payload.isc_key_box.id = urls[3]; // id 추가.
      } else if (url.indexOf("app/push") === 0) type = "appPushStore/setItem";
    }

    return type;
  }

  // Send a message to device
  // Mobile -> Mobile Mobile & Mobile -> Mobile Mobile 데이터 전송 용도 이다.
  // Mobile -> Mobile Mobile : 분산 서버 환경일땐 로드 밸런서 에서 Client IP 로 세션 연관성 설정 해서 동일 업소에 장비를 동일 서버에 연결 한다.
  // Mobile -> Mobile Mobile : Socket client-web 으로 서버에서 수신한 데이터를 동일 업소 Mobile 에 전송한다.
  broadcast(json, sender) {
    // If there are no wss, then don't broadcast any messages
    if (this.clients.length === 0) {
      console.log("\n");
      return;
    }

    console.log("\n\n");
    console.log("--------------------------------------------");
    console.log("------------- brodcast to mobile clients ");
    console.log("--------------------------------------------");

    console.log("-> Mobile clients ", this.clients.length);

    let msg = JSON.stringify(json);
    msg = msg.length > 1000 ? msg.substring(0, 1000) + " ....more..." : msg;

    console.log("-> Mobile json :", chalk.grey(msg));

    let { type, token, payload, place_id, headers, body } = json;

    let { channel, token: _token, url, uuid, req_channel = "mobile" } = headers || {};

    let message = {
      websocket: true,
      token: token || _token,
      type: this.getType(url, payload || body),
      payload: payload || body,
      metadata: {
        uuid,
        req_channel,
        createdAt: Date.now(), // 현재 일시 추가.
      },
    };

    console.log("- ", { place_id, uuid, req_channel });

    if (!message.token) {
      console.log("-> Mobile token 값이 없습니다!", message);
      console.log("\n");
      return;
    }

    if (!place_id && headers.place_id) place_id = headers.place_id;
    if (!place_id && message.payload.place_id) place_id = message.payload.place_id;
    if (!place_id && sender) place_id = sender.place_id;

    if (place_id === undefined) {
      console.log("-> Mobile place_id 가 없습니다!", message);
      console.log("\n");
      return;
    }

    if (!message.type) {
      console.log("-> Mobile message type 이 없습니다!", message);
      console.log("\n");
      return;
    }

    if (!message.token) {
      console.log("-> Mobile token 값이 없습니다!", message);
      console.log("\n");
      return;
    }

    if (!place_id && sender) place_id = sender.place_id;

    if (!place_id) {
      console.log("-> Mobile place_id 가 없습니다!", message);
      console.log("\n");
      return;
    }

    // console.log("");
    // console.log(moment().format("YYYY-MM-DD HH:mm:ss"), "\n", "-> Mobile sender place_id", place_id, message.type, "\n", "-> Mobile uuid", uuid, "\n", JSON.stringify(message.payload), "\n");

    this.clients.forEach((ws) => {
      if (ws.readyState === WebSocket.OPEN) {
        const { place_id: _place_id, uuid: _uuid } = ws;

        if (_place_id) {
          // console.log("-> Mobile ws place_id", _place_id, " uuid", _uuid);

          // 동일 업소만 업데이트 한다.(중요!! 다른 업소에 정보가 노출 되면 안됨!)
          if (Number(place_id) === Number(_place_id)) {
            // 동일 업소의 다른 사용자만 업데이트 한다.
            // 전송 채널이 MOBILE 이 아니라면(device 등) uuid 에 상관없이 전파.
            if (req_channel !== "mobile" || !uuid || uuid !== _uuid || message.type === "ADD_ROOM_STATE_LOG_LIST") {
              console.log(chalk.green("====> Mobile send to client place_id:", _place_id, " uuid", _uuid));
              ws.send(JSON.stringify(message));
            }
          }
        }
      }
    });

    return;
  }
}

module.exports = MobileServer;
