const _ = require("lodash");

/** 오류 정의 */
const ERROR = {
  DEFAULT: {
    code: 400,
    message: "요청 처리중 오류가 발생 했습니다.",
    detail: "관리자에게 문의해 주세요."
  },
  INVALID_PARAMETER: {
    code: 400,
    message: "요청 정보가 올바르지 않습니다.",
    detail: []
  },
  NO_DATA: {
    code: 400,
    message: "요청 정보가 존재 하지 않습니다.",
    detail: "요청 정보 확인 후 다시 시도해 주세요."
  },
  INVALID_ARGUMENT: {
    code: 400,
    message: "입력 정보가 올바르지 않습니다.",
    detail: "입력 정보 확인 후 다시 시도해 주세요."
  },
  INVALID_USER: {
    code: 401,
    message: "유효한 사용자가 아닙니다.",
    detail: "관리자에게 문의해 주세요."
  },
  NO_CERTIFICATION: {
    code: 401,
    message: "인증 정보가 없습니다.",
    detail: "인증 후 다시 시도해 주세요."
  },
  INVALID_CERTIFICATION: {
    code: 401,
    message: "인증 정보가 유효하지 않습니다.",
    detail: "관리자에게 문의해 주세요."
  },
  REQUIRE_CERTIFICATION: {
    code: 401,
    message: "필수 인증 정보가 없습니다.",
    detail: "업소 아이디를 넣어 주세요."
  },
  EXPIRE_CERTIFICATION: {
    code: 401,
    message: "인증 기간이 만료 되었습니다.",
    detail: "관리자에게 문의해 주세요."
  },
  INVALID_AUTHORITY: {
    code: 401,
    message: "접근 권한이 없습니다.",
    detail: "로그인 후 다시 시도해 주세요."
  },
  ALREADY_INTERRUPT: {
    code: 403,
    message: "다른 고객이 사용 중 입니다.",
    detail: "잠시 후 다시 시도해 주세요."
  }
};

module.exports = { ERROR };
